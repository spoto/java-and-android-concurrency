\documentclass[11pt]{beamer}  %% versione proiettore
%%\documentclass[11pt,handout]{beamer} %% versione stampa
\usepackage{lucidiJb-2ed}
\usepackage[normalem]{ulem}

\mode<article>
{
  \usepackage{fullpage}
  \usepackage{hyperref}
}

\mode<presentation>
{
  \setbeamertemplate{background canvas}[vertical shading][bottom=red!10,top=blue!10]
  \usetheme{Lambdas}
  \usefonttheme[onlysmall]{structurebold}
}

\title{Lambdas and Streams}
\author{Fausto Spoto}
\subtitle{Introduction to Java}
\institute{Universit\`a di Verona, Italy}
\date{December 2018}

\setbeamercovered{invisible}

\begin{document}

\frame{\titlepage}

\begin{frame}[fragile]\frametitle{A simple class that prints the first primes}

{\small\begin{alltt}
public class Primes \{
  public void process(int howMany) \{
    for (long next = 2L; howMany > 0; howMany--, next++) \{
      while (!isPrime(next))
        next++;

      \alert{System.out.println(next);}
    \}
  \}

  private boolean isPrime(long n) \{
    for (long d = 2L; d * d <= n; d++)
      if (n % d == 0L)
        return false;

    return true;
  \}
\}
\end{alltt}}

\end{frame}

\begin{frame}[fragile]\frametitle{What if we want to write the primes into a file?}

{\small\begin{alltt}
public class Primes \{
  public void process(int howMany) \{
    \alert{try (PrintWriter pw = new PrintWriter("primes.txt")) \{}
      for (long next = 2L; howMany > 0; howMany--, next++) \{
        while (!isPrime(next))
          next++;

        \sout{System.out.println(next);}
        \alert{pw.println(next);}
      \}
    \alert{\}}
    \alert{catch (IOException e) \{}
      \alert{System.out.println("something went wrong!");}
    \alert{\}}
  \}

  private boolean isPrime(long n) \{ ... \}
\}
\end{alltt}}

\end{frame}

\begin{frame}\frametitle{What if\ldots}

  \begin{itemize}
  \item \ldots we want to send the primes to the Internet
  \item \ldots we want to print the primes on the same line
  \item \ldots we want ro print only a prime over two
  \item \ldots we want to show the primes on a graphical interface
  \item \ldots we want to add all the primes
  \item \ldots
  \end{itemize}

  \medskip
  \begin{greenbox}{}
    Writing a class for each case is complex and becomes a maintenance problem
  \end{greenbox}

  \medskip
  \begin{greenbox}{}
    An abstract method wrapping the different implementation strategy might help, but we cannot use the same object with different strategies
  \end{greenbox}
  
\end{frame}

\begin{frame}[fragile]\frametitle{The Hollywood principle: Don't call me, I'll call you}

{\small\begin{alltt}
\alert{import java.util.function.Consumer;}

public class Primes \{
  public void process(int howMany, \alert{Consumer<Long> strategy}) \{
    for (long next = 2L; howMany > 0; howMany--, next++) \{
      while (!isPrime(next))
        next++;

      \alert{strategy.accept(next);}
    \}
  \}

  private boolean isPrime(long n) \{ ... \}
\}
\end{alltt}}

\begin{greenbox}{}
{\small\begin{alltt}
public interface Consumer<T> \{
  void accept(T);
\}
\end{alltt}}
\end{greenbox}
  
\end{frame}

\begin{frame}[fragile]\frametitle{Implementing \texttt{Consumer} with anonymous classes}

{\small\begin{alltt}
public static void main(String[] args) \{
  Primes primes = new Primes();
  primes.process(20, \alert{new Consumer<Long>() \{
    public void accept(Long prime) \{
      System.out.println(prime);
    \}
  \}});
  try (PrintWriter pw = new PrintWriter("primes.txt")) \{
    primes.process(20, \alert{new Consumer<Long>() \{
      public void accept(Long prime) \{
        pw.println(prime);
      \}
    \}});
  \}
  catch (IOException e) \{
    System.out.println("something went wrong!");
  \}
\}
\end{alltt}}

\end{frame}

\begin{frame}[fragile]\frametitle{Boilerplate everywhere}

{\small\begin{alltt}
public static void main(String[] args) \{
  Primes primes = new Primes();
  primes.process(20, \alert{\sout{new Consumer<Long>() \{}
    \sout{public void accept(Long} prime\sout{) \{}
      System.out.println(prime);
    \sout{\}}
  \sout{\}}});
  try (PrintWriter pw = new PrintWriter("primes.txt")) \{
    primes.process(20, \alert{\sout{new Consumer<Long>() \{}
      \sout{public void accept(Long} prime\sout{) \{}
        pw.println(prime);
      \sout{\}}
    \sout{\}}});
  \}
  catch (IOException e) \{
    System.out.println("something went wrong!");
  \}
\}
\end{alltt}}

\end{frame}

\begin{frame}[fragile]\frametitle{Enter the lambda}

{\small\begin{alltt}
public static void main(String[] args) \{
  Primes primes = new Primes();
  primes.process(20, \alert{prime -> System.out.println(prime)});



  
  try (PrintWriter pw = new PrintWriter("primes.txt")) \{
    primes.process(20, \alert{prime -> pw.println(prime)});




  \}
  catch (IOException e) \{
    System.out.println("something went wrong!");
  \}
\}
\end{alltt}}

\end{frame}

\begin{frame}[fragile]\frametitle{What is a lambda?}

  \begin{greenbox}{You can see a lambda as an implementation of an interface}
    \begin{alltt}
Consumer<String> x = s -> System.out.println(s.substring(1));
    \end{alltt}
    \vspace*{-3ex}
    The interface must have exactly one method (\emph{functional} interface)
  \end{greenbox}

  \bigskip
  \begin{greenbox}{You can see a lambda as a pointer to a function}
    \begin{alltt}
Consumer<Long> x = prime -> System.out.println(prime);
primes.process(30, x);
    \end{alltt}
  \end{greenbox}
\end{frame}

\begin{frame}[fragile]\frametitle{Syntax of lambdas: Parameters}

  One parameter:
  \begin{alltt}
    p -> p.translate(1, 1)
    i -> new Point(i, i + 1)
  \end{alltt}

  Two parameters:
  \begin{alltt}
    (x, y) -> x + y
    (x, y) -> x.concat(y)
  \end{alltt}

  No parameters:
  \begin{alltt}
    () -> 42
  \end{alltt}
\end{frame}

\begin{frame}[fragile]\frametitle{Syntax of lambdas: Argument types and return value}

  Types \alert{can} be specified:
  \begin{alltt}
    (Thread t) -> \{ t.start(); \}
    (int i1, long i2) -> i1 + i2
  \end{alltt}
  Otherwise they are \alert{inferred} from the context

  \medskip
  A lambda
  \begin{alltt}
    args -> expression
  \end{alltt}
  is equivalent to
  \begin{alltt}
    args -> \{ \alert{return} expression; \}
  \end{alltt}
\end{frame}

\begin{frame}[fragile]\frametitle{Functional interfaces (SAM-types)}

  A \alert{functional interface} is one with only a single abstract method, such as
  \begin{alltt}
    public interface UnaryOperator<T> \{
      T apply(T t);
    \}
  \end{alltt}
  Lambdas implement functional interfaces and can be used wherever one is
  expected: the following method of \texttt{java.util.List<E>}:
  \begin{alltt}
    public void replaceAll(UnaryOperator<E> operator)
  \end{alltt}
  can be used as follows:
  \begin{alltt}
    List<Integer> l = ...;
    l.replaceAll(i -> i + 1);
    l.replaceAll(i -> i * i);
  \end{alltt}
\end{frame}

\begin{frame}[fragile]\frametitle{Package \texttt{java.util.function}}
  \begin{center}
  \begin{tabular}{l|l|l|l}
    \textbf{Interface} & \textbf{Args.} & \textbf{Returns} & \textbf{Example}\\\hline\hline
    \texttt{Consumer<T>} & \texttt{T} & \texttt{void} & \texttt{s -> System.out.print(s)}\\\hline
    \texttt{Predicate<T>} & \texttt{T} & \texttt{boolean} & \texttt{s -> s.isEmpty()}\\\hline
    \texttt{Supplier<T>} & none & \texttt{T} & \texttt{() -> new String()}\\\hline
    \texttt{Function<T,U>} & \texttt{T} & \texttt{U} & \texttt{s -> new Integer(s)}\\\hline
  \end{tabular}
  \end{center}

  \bigskip
  You are free to define your own functional interfaces, but use those above
  if you can, for standardization
\end{frame}

\begin{frame}\frametitle{External and internal iteration}

  \begin{center}
    \includegraphics[width=11.5cm]{pictures/external-internal.jpg}
  \end{center}
  
Internal iteration is simpler, declarative, does not create any iterator and can be more efficient, by accessing the internal structure of the iterated collection, directly

\end{frame}

\begin{frame}[fragile]\frametitle{From external to internal iteration}
\begin{alltt}
List<String> l = ... ;
\end{alltt}

\begin{greenbox}{External Iteration}
We extract the elements of \texttt{l} and process each of them:    
\begin{alltt}
for (String s: l)
  process(s);
\end{alltt}
\end{greenbox}

\bigskip
\begin{greenbox}{Internal Iteration}
We provide the task that must be applied to each element of \texttt{l}
\begin{alltt}
  l.forEach(s -> process(s));
\end{alltt}
\end{greenbox}

\end{frame}

\begin{frame}[fragile]\frametitle{Examples of internal iteration}

\begin{alltt}
List<String> l = ... ;
Set<String> copy = new HashSet<>();  
\end{alltt}

\begin{greenbox}{\emph{Print all elements of the list}}
\begin{alltt}
l.forEach(s -> System.out.println(s));
\end{alltt}
\end{greenbox}

\medskip

\begin{greenbox}{\emph{Add all elements to the \texttt{copy}}}
\begin{alltt}
l.forEach(s -> copy.add(s));
\end{alltt}
\end{greenbox}

\medskip

\begin{greenbox}{\emph{For each element, create an instance of \texttt{Processor}}}
\begin{alltt}
l.forEach(s -> new Processor(s));
\end{alltt}
\end{greenbox}

\medskip

\begin{greenbox}{\emph{For each element, call static method \texttt{Util.digest}}}
\begin{alltt}
l.forEach(s -> Util.digest(s));
\end{alltt}
\end{greenbox}

\end{frame}

\begin{frame}[fragile]\frametitle{Examples of internal iteration}

\begin{alltt}
List<String> l = ... ;
Set<String> copy = new HashSet<>();  
\end{alltt}

\begin{greenbox}{\emph{Add to \texttt{copy} all elements of length at least 42}}
\begin{alltt}
l.forEach(s ->
  if (s.length() >= 42)
    copy.add(s)
);
\end{alltt}
\end{greenbox}

\medskip

\begin{greenbox}{\emph{For each element, if its length is at least 42, add it to \texttt{copy}. In any case, print all elements}}
{\small\begin{alltt}
l.forEach(s -> \alert{\{}
  if (s.length() >= 42)
    copy.add(s);

  System.out.println(s);
\alert{\}});
\end{alltt}}
\end{greenbox}

\medskip

\begin{greenbox}{\emph{For each element, call static method \texttt{Util.digest}}}
\begin{alltt}
l.forEach(s -> Util.digest(s));
\end{alltt}
\end{greenbox}

\end{frame}

\begin{frame}[fragile]\frametitle{Method and constructor references}

  For compactness and efficiency, it is possible to replace
  \begin{alltt}
    list.forEach(s -> System.out.println(s));
    list.forEach(s -> copy.add(s));
    list.forEach(s -> new Processor(s));
    list.forEach(s -> Util.digest(s));
  \end{alltt}
  with
  \begin{alltt}
    list.forEach(\alert{System.out::println});  // method reference
    list.forEach(\alert{copy::add}); // method reference
    list.forEach(\alert{Processor::new}); // constructor reference
    list.forEach(\alert{Util::digest}); // method reference
  \end{alltt}
  
\end{frame}

\begin{frame}\frametitle{Method and constructor references}

  \begin{center}
    \begin{scriptsize}
      \hspace*{-2ex}\begin{tabular}{l|l|c}
        \textbf{Name} & \textbf{Syntax} & \textbf{Lambda Equivalent}\\\hline\hline
        Static & \texttt{ClasName::staticMethod} & \texttt{(args) -> ClasName.staticMethod(args)} \\\hline
        Bound Instance & \texttt{expr::instMethod} & \texttt{(args) -> expr.instMethod(args)} \\\hline
        Unbound Instance & \texttt{ClsName::instMethod} & \texttt{(arg0,rest) -> arg0.instMethod(rest)} \\\hline
        Constructor & \texttt{ClsName::new} & \texttt{(args) -> new ClsName(args)} \\\hline
        Array Creator & \texttt{BasicType$\underbrace{\mathtt{[]}\cdots\mathtt{[]}}_{\textit{n\ times}}$::new} & \texttt{(i1,\ldots,in) -> new BasicType[i1]\ldots[in]} \\\hline
      \end{tabular}
      \end{scriptsize}
  \end{center}

\end{frame}

\begin{frame}[fragile]\frametitle{Using method references}

{\small\begin{alltt}
public static void main(String[] args) \{
  Primes primes = new Primes();
  primes.process(20, \sout{prime -> System.out.println(prime)});



  
  try (PrintWriter pw = new PrintWriter("primes.txt")) \{
    primes.process(20, \sout{prime -> pw.println(prime)});




  \}
  catch (IOException e) \{
    System.out.println("something went wrong!");
  \}
\}
\end{alltt}}

\end{frame}

\begin{frame}[fragile]\frametitle{Using method references}

{\small\begin{alltt}
public static void main(String[] args) \{
  Primes primes = new Primes();
  primes.process(20, \alert{System.out::println});



  
  try (PrintWriter pw = new PrintWriter("primes.txt")) \{
    primes.process(20, \alert{pw::println});




  \}
  catch (IOException e) \{
    System.out.println("something went wrong!");
  \}
\}
\end{alltt}}

\end{frame}

\begin{frame}\frametitle{Streams: \texttt{java.util.stream.Stream<T>}}
  \begin{greenbox}{A stream is a description of a lazy sequence of values}
    A stream is not a collection: it has no elements, it can even be infinite

    Do not get confused with \texttt{FileInputStream} or similar: a stream is not necessarily for input/output
  \end{greenbox}

  \bigskip
  You can create streams from many sources:
  \begin{itemize}
  \item from a collection \texttt{c}: \texttt{c.stream()}
  \item from elements or arrays: \texttt{Stream.of(a, b, c,\ldots)} or \texttt{Stream.of(array)}
  \item from a first element and next function: \texttt{Stream.iterate(first, next)}
  \item from an iterable: \texttt{StreamSupport.stream(iterable.spliterator(), false)}
  \item from other data containers: for instance \texttt{BufferedReader.lines()} yields a stream of strings
  \end{itemize}
\end{frame}

\begin{frame}\frametitle{Stream pipeline (create/filter/map/reduce)}
%
\begin{center}
\includegraphics[width=11.5cm]{pictures/stream-pipeline.png}
\end{center}
%
\begin{center}
Only the terminal operation runs the pipeline! (\alert{lazy} streams)
\end{center}

\end{frame}

\begin{frame}\frametitle{Filter and map}
  
\begin{center}
  \includegraphics[width=7cm]{pictures/filter.png}
\end{center}
\bigskip
\begin{center}
  \includegraphics[width=7cm]{pictures/map.png}
\end{center}

\end{frame}

\begin{frame}[fragile]\frametitle{Examples of stream pipelines}

\begin{alltt}
Collection<String> c = ... ;
\end{alltt}

\begin{greenbox}{\emph{Count how many elements of \texttt{c} start with \texttt{"a"}}}
\begin{alltt}
long howMany = c.stream()
                .filter(s -> s.startsWith("a"))
                .count();
\end{alltt}
\end{greenbox}

\medskip

\begin{greenbox}{\emph{Print all elements of \texttt{c}, capitalized}}
\begin{alltt}
c.stream()
 .map(s -> s.toUpperCase())
 .forEach(s -> System.out.println(s));
\end{alltt}
or equivalently with method references:
\begin{alltt}
c.stream()
 .map(String::toUpperCase)
 .forEach(System.out::println);
\end{alltt}
\end{greenbox}

\end{frame}

\begin{frame}[fragile]\frametitle{Examples of stream pipelines}

\begin{greenbox}{\emph{Print the elements of \texttt{c} of length at least $42$, capitalized}}
\begin{alltt}
c.stream()
 .filter(s -> s.length() >= 42)
 .map(s -> s.toUpperCase())
 .forEach(s -> System.out.println(s));
\end{alltt}

or equivalently with method references:

\begin{alltt}
c.stream()
 .filter(s -> s.length() >= 42)
 .map(String::toUpperCase)
 .forEach(System.out::println);
\end{alltt}
\end{greenbox}

\end{frame}

\begin{frame}[fragile]\frametitle{Examples of stream pipelines}

\begin{greenbox}{\emph{Find an element of \texttt{c} that starts with \texttt{"a"} and is at least $42$ characters long}}
\begin{alltt}
Optional<String> found = c.stream()
                          .filter(s -> s.startsWith("a"))
                          .filter(s -> s.length() >= 42)
                          .findAny();

if (found.isPresent())
  System.out.println("here it is: " + found.get());
\end{alltt}
\end{greenbox}

\end{frame}

\begin{frame}[fragile]\frametitle{Examples of stream pipelines}

\begin{greenbox}{\emph{Check if all elements of \texttt{c} are at least $42$ characters long}}
\begin{alltt}
boolean allAtLeast42 = c.stream()
                        .allMatch(s -> s.length() >= 42);
\end{alltt}
\end{greenbox}

\bigskip

\begin{greenbox}{\emph{Check if at least an element of \texttt{c} is at least $42$ characters long}}
\begin{alltt}
boolean oneAtLeast42 = c.stream()
                        .anyMatch(s -> s.length() >= 42);
\end{alltt}
\end{greenbox}

\bigskip

\begin{greenbox}{\emph{Check if no element of \texttt{c} is at least $42$ characters long}}
\begin{alltt}
boolean noneAtLeast42 = c.stream()
                         .noneMatch(s -> s.length() >= 42);
\end{alltt}
\end{greenbox}

\end{frame}

\begin{frame}[fragile]\frametitle{Santa is a person who brings presents}

{\small\begin{alltt}
public class \alert{Person} \{
  private final String country;
  public Person(String country)
  public String getCountry() \{
    return country;
  \}
\}

public class \alert{Santa extends Person} \{
  private final Set<String> presents;
  public Santa(String country, Set<String> presents) \{
    super(country);
    this.presents = presents;
  \}
  public Stream<String> getPresents() \{
    return presents.stream();
  \}
\}
\end{alltt}}

\end{frame}

\begin{frame}[fragile]\frametitle{Examples of stream pipelines}

\begin{alltt}
Collection<Person> c = ...;
\end{alltt}
  
\begin{greenbox}{\emph{Count how many Santas are in \texttt{c}}}
\begin{alltt}
long howMany =
  c.stream()
   .filter(person -> person instanceof Santa)
   .count();
\end{alltt}
\end{greenbox}

\bigskip

\begin{greenbox}{\emph{Count how many Santas from Finland are in \texttt{c}}}
\begin{alltt}
long howMany =
  c.stream()
   .filter(person -> person instanceof Santa)
   .filter(person -> person.getCountry().equals("Finland"))
   .count();
\end{alltt}
\end{greenbox}

\end{frame}

\begin{frame}\frametitle{Distinct}
  
\begin{center}
  \includegraphics[width=10cm]{pictures/distinct.png}
\end{center}

\end{frame}

\begin{frame}[fragile]\frametitle{Changing type of the stream}

\begin{greenbox}{\emph{Print the countries of all Santas in \texttt{c}, without repetitions}}

{\small\begin{alltt}
c.stream() // \alert{Stream<Person>}
 .filter(person -> person instanceof Santa) // \alert{Stream<Person>}
 .map(Person::getCountry) // \alert{Stream<String>}
 .\alert{distinct}() // \alert{Stream<String>}
 .forEach(System.out::println);
\end{alltt}}

\end{greenbox}

\bigskip

\begin{greenbox}{\emph{Check if at least a Santa in \texttt{c} lives in Finland}}

{\small\begin{alltt}
boolean isSantaInFinland =
  c.stream() // \alert{Stream<Person>}
   .filter(person -> person instanceof Santa) // \alert{Stream<Person>}
   .map(Person::getCountry) // \alert{Stream<String>}
   .\alert{anyMatch}(country -> "Finland".equals(country));
\end{alltt}}
\end{greenbox}

where the last line can simplified into

\begin{greenbox}{}
{\small\begin{alltt}
   \ldots.\alert{anyMatch}("Finland"::equals);
\end{alltt}}
\end{greenbox}

\end{frame}

\begin{frame}\frametitle{Map vs flat map}
  
\begin{center}
  \includegraphics[width=7cm]{pictures/map.png}
\end{center}
\bigskip
\begin{center}
  \includegraphics[width=7cm]{pictures/flatmap.png}
\end{center}

\end{frame}

\begin{frame}[fragile]\frametitle{Merging streams with \texttt{flatMap}}

\begin{greenbox}{\emph{Print the names of all presents brought by the Santas in \texttt{c}, without repetitions and in ascending alphabetical order}}

{\small\begin{alltt}
c.stream() // \alert{Stream<Person>}
 .filter(person -> person instanceof Santa) // \alert{Stream<Person>}
 .map(person -> (Santa) person) // \alert{Stream<Santa>}
 .\alert{flatMap}(Santa::getPresents) // \alert{Stream<String>}
 .distinct() // \alert{Stream<String>}
 .sorted() // \alert{Stream<String>}
 .forEach(System.out::println);
\end{alltt}}
\end{greenbox}

\end{frame}

\begin{frame}[fragile]\frametitle{Parallel streams}
  
\begin{greenbox}{\emph{Count how many presents bring the Santas in \texttt{c}}}

{\small\begin{alltt}
long howMany =
  c.\alert{stream}() // \alert{Stream<Person>}
   .filter(person -> person instanceof Santa) // \alert{Stream<Person>}
   .map(person -> (Santa) person) // \alert{Stream<Santa>}
   .map(Santa::getPresents) // \alert{Stream<Stream<String>>}
   .mapToLong(Stream::count) // \alert{LongStream}
   .sum();
\end{alltt}}
\end{greenbox}

\bigskip

\begin{greenbox}{\emph{Do the same in parallel}}

{\small\begin{alltt}
long howMany =
  c.\alert{parallelStream}() // \alert{Stream<Person>}
   .filter(person -> person instanceof Santa) // \alert{Stream<Person>}
   .map(person -> (Santa) person) // \alert{Stream<Santa>}
   .map(Santa::getPresents) // \alert{Stream<Stream<String>>}
   .mapToLong(Stream::count) // \alert{LongStream}
   .sum();
\end{alltt}}
\end{greenbox}

\end{frame}

\begin{frame}[fragile]\frametitle{Streams of primitive values}

  \begin{greenbox}{\texttt{LongStream} vs.\ \texttt{Stream<Long>}}
    \texttt{LongStream} is a stream of primitive \texttt{long} values:
    \begin{itemize}
    \item it does not use boxing/unboxing $\Rightarrow$ more efficient
    \item it has specialized methods (\texttt{min()}, \texttt{max()}, \texttt{sum()}, \ldots)
    \item it has static methods for building ranges of values
    \end{itemize}
  \end{greenbox}

  \bigskip
  Similarly, there are classes \texttt{IntStream} and \texttt{DoubleStream}

  \bigskip
  \begin{greenbox}{How to impress your friends}
\begin{alltt}
private boolean isPrime(long n) \{
  return LongStream.range(2L, (long) Math.sqrt(n) + 1L)
    .noneMatch(d -> n % d == 0L);
\}
\end{alltt}
  \end{greenbox}
\end{frame}

\begin{frame}\frametitle{Scope and name capture}

  \alert{Lambdas define a block scope} hence, as always in Java:
  \begin{itemize}
  \item local names shadow field names
  \item local names cannot shadow already existing external local names
  \item external names can be used inside a lambda (but only if they
    are \alert{effectively \texttt{final}})
  \end{itemize}

  \bigskip
  \alert{Lambdas have no object scope}, hence
  \begin{itemize}
  \item \texttt{this} refers to the same \texttt{this} as in its external scope,
    \alert{not to the lambda itself}
  \item \texttt{super} refers to the superclass of the class where the lambda
    is defined, \alert{not to the superinterface of the functional interface}
  \end{itemize}
  
\end{frame}

\begin{frame}[fragile]\frametitle{Effectively \texttt{final} variables}

  \begin{greenbox}{This code is correct, since variable \texttt{set} is effectively \texttt{final}}
  {\small\begin{alltt}
    Set<String> set = ...  // you could write final here
    List<String> toAdd = ...
    toAdd.forEach(\alert{set}::add);
  \end{alltt}}
  \end{greenbox}

  \medskip
  \begin{greenbox}{This code does not compile instead}
  {\small\begin{alltt}
    Set<String> set = ...
    List<String> toAdd = ...
    \alert{set = new HashSet<>(set);}
    toAdd.forEach(\alert{set}::add);
  \end{alltt}}
  \end{greenbox}

  \medskip
  \begin{greenbox}{This code does not compile either}
  {\small\begin{alltt}
    Set<String> set = ...
    List<String> toAdd = ...
    toAdd.forEach(\alert{set}::add);
    \alert{set = new HashSet<>(set);}
  \end{alltt}}
  \end{greenbox}

\end{frame}

\begin{frame}
\frametitle{Nightly read}
\begin{greenbox}{}
\begin{center}
\includegraphics[width=4cm]{pictures/java_8_lambdas.jpg}
\hspace*{2cm}
\includegraphics[width=5.2cm]{pictures/mastering_lambdas.jpg}
\end{center}
\end{greenbox}

\end{frame}

\end{document}
