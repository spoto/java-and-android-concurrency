\documentclass[11pt]{beamer}  %% versione proiettore
%%\documentclass[11pt,handout]{beamer} %% versione stampa
\usepackage{lucidiJb-2ed}

\mode<article>
{
  \usepackage{fullpage}
  \usepackage{hyperref}
}

\mode<presentation>
{
  \setbeamertemplate{background canvas}[vertical shading][bottom=red!10,top=blue!10]
  \usetheme{Concurrency}
  \usefonttheme[onlysmall]{structurebold}
}

\title{Basic Java Concurrency}
\author{Fausto Spoto}
\subtitle{The least you must know about concurrency in Java}
\institute{Universit\`a di Verona, Italy}
\date{December 2018}

\setbeamercovered{invisible}

\begin{document}

\frame{\titlepage}

\begin{frame}
\frametitle{Why concurrency matters}
\begin{itemize}
\item on monocore architectures, it allows one to
      keep the processor busy
      \begin{itemize}
      \item hence its use in all operating systems
      \item and its presence in programming languages: C, Java etc
      \end{itemize}
\item on multicore architectures, it also allows one
      to use all computing cores to solve a single problem
      \begin{itemize}
      \item getting important nowadays
      \end{itemize}
\end{itemize}

\begin{center}
\includegraphics[width=4cm]{pictures/multicore.jpg}
\hspace*{1cm}
\includegraphics[width=4cm]{pictures/shared_resource.jpg}
\end{center}

\end{frame}

\begin{frame}
\frametitle{Why concurrency matters more and more}

\begin{greenbox}{Modern frameworks are inherently multithreaded}
  Servlet containers run each request in a separate thread.
  You might be writing concurrent programs without even
  creating a single thread in your code!
\end{greenbox}

\bigskip

\begin{greenbox}{GUIs must be responsive}
\begin{itemize}
\item code in the user interface thread must terminate quickly,
      or otherwise the user experience gets degraded: long running
      tasks must be delegated to non-user interface threads
\item while a task is running, a mobile user might need to
      start another app, make a call, surf the internet etc.
      The running task must continue in the background
\end{itemize}
\end{greenbox}

\end{frame}

\begin{frame}
\frametitle{Why concurrency is difficult}
\begin{itemize}
\item protocols might be wrong
      \begin{itemize}
      \item race conditions
      \item deadlocks
      \end{itemize}
\item protocols might be inefficient
      \begin{itemize}
      \item livelocks
      \item slower than monothreaded!
      \end{itemize}
\item data can be shared
\item data can be modified
\item data can be cached on each single core
      \begin{itemize}
      \item each core might have a different view of the memory
      \end{itemize}
\end{itemize}

\begin{redbox}{}
  \begin{center}
    Programmers are not enough qualified to deal with concurrency
  \end{center}
\end{redbox}

\end{frame}

\begin{frame}
\frametitle{Concurrency in Java}
\begin{itemize}
\item from Java 1.0:
      \begin{itemize}
      \item \emph{multithreading}: all threads share the same heap
      \item a thread extends \texttt{java.lang.Thread} and overrides \texttt{run}
      \item a thread is relatively slow to start
      \item \texttt{synchronized} blocks and methods
      \item each object has a lock accessible through
            \texttt{wait}/\texttt{notify}/\texttt{notifyAll}
      \item \texttt{volatile} variables are never kept into caches
      \item a formal Java memory model
      \end{itemize}
\item from Java 1.5:
      \begin{itemize}
      \item improved memory model
      \item \texttt{java.lang.concurrent.*} has many clever classes
            for concurrency:
            concurrent hashmaps, latches, futures, callables
      \item executors recycle threads to avoid their startup cost
      \end{itemize}
\item from Java 1.7:
      \begin{itemize}
      \item fork executors share tasks across threads for divide and conquer
      \end{itemize}
\item from Java 1.8:
  \begin{itemize}
    \item parallel streams with lambda expressions as tasks
  \end{itemize}
\end{itemize}
\end{frame}

\begin{frame}[fragile]
\frametitle{Task executor and task specification}

\begin{center}
  \includegraphics[width=3.6cm]{pictures/runnable_thread.png}
\end{center}

\end{frame}

\begin{frame}[fragile]
  \frametitle{Creation of a thread}

  A thread can be created by subclassing the \texttt{java.lang.Thread} class:
\begin{alltt}
 public class MyThread extends Thread \{
   @Override
   public void run() \{ ... do something here ... \}
 \}
 ...
 new MyThread().start();
\end{alltt}

A thread can also be created by specifying the task in the constructor
of a new \texttt{java.lang.Thread}:
\begin{alltt}
 public class \alert{MyRunnable} implements Runnable \{
   @Override
   public void run() \{ ... do something here ... \}
 \}
 ...
 new Thread(new \alert{MyRunnable}()).start();
\end{alltt}

\end{frame}

\begin{frame}\frametitle{The lifecycle of a thread}

\begin{center}
  \includegraphics[width=11cm]{pictures/thread-lifecycle.png}
\end{center}

\end{frame}

\begin{frame}\frametitle{Threads are dynamically scheduled on cores}

\begin{center}
  \includegraphics[width=11cm]{pictures/thread-scheduling.png}
\end{center}

\end{frame}

\begin{frame}[fragile]
\frametitle{The meaning of \texttt{synchronized}}

The compiler translates

\begin{greenbox}{}
\begin{verbatim}
synchronized (expression) {
  body
}
\end{verbatim}
\end{greenbox}

into
\medskip

\begin{greenbox}{}
\begin{verbatim}
final temp = expression;
get the lock of temp
body
release the lock of temp
\end{verbatim}
\end{greenbox}

\medskip

\begin{redbox}{}
\texttt{temp} is constant hence lock and unlock are paired
\end{redbox}

\end{frame}

\begin{frame}[fragile]
\frametitle{The meaning of a \texttt{synchronized} instance method}

The compiler translates

\begin{greenbox}{}
\begin{verbatim}
synchronized T foo(pars) { body }
\end{verbatim}
\end{greenbox}

into
\medskip

\begin{greenbox}{}
\begin{verbatim}
T foo(pars) {
  synchronized (this) { body }
}
\end{verbatim}
\end{greenbox}

that is into
\medskip

\begin{greenbox}{}
\begin{verbatim}
T foo(pars) {
  get the lock of this
  body
  release the lock of this
}
\end{verbatim}
\end{greenbox}

\medskip

\begin{redbox}{}
``\texttt{this}'' is constant in Java hence lock and unlock are paired
\end{redbox}

\end{frame}

\begin{frame}[fragile]
\frametitle{The meaning of a \texttt{synchronized} static method of class \texttt{C}}

The compiler translates

\begin{greenbox}{}
\begin{verbatim}
synchronized static T foo(pars) { body }
\end{verbatim}
\end{greenbox}

into
\medskip

\begin{greenbox}{}
\begin{verbatim}
static T foo(pars) {
  synchronized (C.class) { body }
}
\end{verbatim}
\end{greenbox}

that is into
\medskip

\begin{greenbox}{}
\begin{verbatim}
T foo(pars) {
  get the lock of C.class
  body
  release the lock of C.class
}
\end{verbatim}
\end{greenbox}

\medskip

\begin{redbox}{}
\texttt{C.class} is constant in Java hence lock and unlock are paired
\end{redbox}

\end{frame}

\begin{frame}[fragile]\frametitle{Java locks are reentrant}

\begin{greenbox}{A thread can acquire a lock as many times as it wants}

\begin{verbatim}
public class C {
  public synchronized void foo() {
    goo(); // fine!
    System.out.println("inside foo()");
  }

  public synchronized void goo() {
    System.out.println("inside goo()");
  }
}
\end{verbatim}
\end{greenbox}

\end{frame}

\begin{frame}
\frametitle{The meaning of \texttt{wait}/\texttt{notify}/\texttt{notifyAll} (historical!)}

\begin{greenbox}{\texttt{expression.wait()}}
  Waits until somebody will notify on the value of \texttt{expression},
  temporarily releasing any lock held by the current thread
\end{greenbox}

\bigskip

\begin{greenbox}{\texttt{expression.notify()}}
Wakes up a thread waiting on the value of \texttt{expression},
if any. If more threads are waiting, one of them
is non-deterministically chosen and awakened
\end{greenbox}

\bigskip

\begin{greenbox}{\texttt{expression.notifyAll()}}
Wakes up all threads waiting on the value of \texttt{expression}, if any.
The awakened threads must recheck the condition they were waiting for
\end{greenbox}

\bigskip

\begin{redbox}{}
These calls must occur only when the thread has already synchronized on the value
of \texttt{expression}
\end{redbox}

\end{frame}

\begin{frame}
\frametitle{Programmers do it wrong}

Typical misconceptions about concurrency:

\bigskip
\begin{greenbox}{``race conditions only occur in books''}
  \texttt{RaceCondition.java} (when racers collide)
\end{greenbox}
\bigskip
\begin{greenbox}{``parallelism increases speed''}
  \texttt{RaceCondition.java} (when one is better than two)
\end{greenbox}
\bigskip
\begin{greenbox}{``deadlocks do not exist in practice''}
  \texttt{Philosophers.java} (when philosophers hang)
\end{greenbox}
\end{frame}

\begin{frame}
\frametitle{The Java memory model}

  It clarifies two aspects: atomicity and visibility
  \begin{enumerate}
  \item<1-> An action should execute as an unbreakable unit
  \item<2> A field write should be visible by everybody
  \end{enumerate}

\only<1>{
\begin{center}
\includegraphics[height=6cm]{pictures/race-condition.png}
\end{center}
}

\only<2>{
\begin{center}
\includegraphics[height=6cm]{pictures/java-memory-model.png}
\end{center}
}

\end{frame}

\begin{frame}\frametitle{Happens-before: single thread rule}

  \begin{greenbox}{}
    Each action in a single thread happens-before every action in that thread
    that comes later in the program order
  \end{greenbox}

  \begin{center}
    \includegraphics[width=7.5cm]{pictures/single-thread-rule.png}
  \end{center}

\end{frame}

\begin{frame}\frametitle{Happens-before: monitor lock rule}

  \begin{greenbox}{}
    An unlock on a monitor lock (existing \texttt{synchronized}) happens-before
    any subsequent acquiring on the same monitor lock
    (entering \texttt{synchronized})
  \end{greenbox}

  \begin{center}
    \includegraphics[width=11cm]{pictures/monitor-lock-rule.png}
  \end{center}

\end{frame}

\begin{frame}
\frametitle{Happens-before: monitor lock rule}

\begin{center}
\includegraphics[width=8cm]{pictures/JMM.png}
\end{center}

\end{frame}

\begin{frame}\frametitle{Happens-before: volatile field rule}

  \begin{greenbox}{}
    A write to a \texttt{volatile} field happens-before every subsequent read of that same field. \alert{\texttt{volatile} guarantees visibility but not atomicity! \texttt{f++} is not atomic!}
  \end{greenbox}

  \begin{center}
    \includegraphics[width=11cm]{pictures/volatile-variable-rule.png}
  \end{center}

\end{frame}

\begin{frame}\frametitle{Happens-before: thread start rule}

  \begin{greenbox}{}
    A call to \texttt{Thread.start()} in a thread $A$ happens-before every action in the started thread $B$
  \end{greenbox}

  \begin{center}
    \includegraphics[width=11cm]{pictures/thread-start-rule.png}
  \end{center}

\end{frame}

\begin{frame}\frametitle{Happens-before: thread join rule}

  \begin{greenbox}{}
    All actions in a thread $B$ happen-before any action in another thread $A$
    after $A$ successfully performs a \texttt{Thread.join()} on $B$.
  \end{greenbox}

  \begin{center}
    \includegraphics[width=11cm]{pictures/thread-join-rule.png}
  \end{center}

\end{frame}

\begin{frame}[fragile]
  \frametitle{Example of (non-)visibility}

  What does this print?

  {\scriptsize
\begin{verbatim}
public class NoVisibility {
  private static boolean ready = false;
  private static int number = 13;

  private static class ReaderThread extends Thread {
    public void run() {
      // leave the processor until ready becomes true
      while (!ready)
        Thread.yield();

      System.out.println(number);
    }
  }

  public static void main(String[] args) {
    new ReaderThread().start();
    number = 42;
    ready = true;
  }
}
\end{verbatim}}
\end{frame}

\begin{frame}
  \frametitle{The Heisenberg principle of concurrent programming}

  \begin{greenbox}{}
    In the absence of synchronization, the compiler, processor, and runtime can do some downright
    weird things to the order in which operations appear to execute. Attempts to reason
    about the order in which memory actions "must" happen in insufficiently
    synchronized multithreaded programs will almost certainly be incorrect
  \end{greenbox}

  \bigskip
  A field might contain any of the values ever written into the field, but not necessarily
  the last one (\alert{out-of-thin-air safety}). For \texttt{long} and \texttt{double}
  fields, even a value never written into the field might be seen!
\end{frame}

\begin{frame}[fragile]
  \frametitle{Synchronization guarantees visibility}

\begin{verbatim}
@NotThreadSafe
public class MutableInteger {
    private int value;

    public int get() {
        return value;
    }

    public void set(int value) {
        this.value = value;
    }
}
\end{verbatim}

\end{frame}

\begin{frame}[fragile]
  \frametitle{Synchronization guarantees visibility}

\begin{alltt}
@NotThreadSafe
public class MutableInteger \{
    private int value;

    public int get() \{
        return value;
    \}

    public \alert{synchronized} void set(int value) \{
        this.value = value;
    \}
\}
\end{alltt}

\end{frame}

\begin{frame}[fragile]
  \frametitle{Synchronization guarantees visibility}

\begin{alltt}
\alert{@ThreadSafe}
public class SynchronizedInteger \{
    private int value; \alert{// guarded by "this"}

    public \alert{synchronized} int get() \{
        return value;
    \}

    public synchronized void set(int value) \{
        this.value = value;
    \}
\}
\end{alltt}

\end{frame}

\begin{frame}[fragile]
  \frametitle{Locking, visibility and \texttt{volatile} fields}

  \begin{greenbox}{Locking guarantees visibility (and atomicity)}
    When thread $A$ executes a \texttt{synchronized} block,
    and subsequently thread $B$ enters a \texttt{synchronized}
    block guarded by the same lock, the values of variables that
    were visible to $A$ prior to realeasing the lock
    are guaranteed to be visible to $B$ upon acquiring the lock
  \end{greenbox}

  \bigskip
  \begin{greenbox}{\texttt{volatile} guarantees visibility only}
    When thread $A$ writes to a volatile field and subsequently thread $B$
    reads the same field, the values of \alert{all} variables that were visible
    to $A$ prior to writing become visible to $B$ after reading
  \end{greenbox}

  {\small
\begin{alltt}
  \alert{volatile} boolean asleep;

  void tryToSleep() {
    while (!asleep)
      countSomeSheep();
  }
\end{alltt}
}

\end{frame}

\begin{frame}
  \frametitle{Use \texttt{volatile} variables sparingly}

  \begin{itemize}
  \item they have higher access cost
  \item compound operations are not atomic: \texttt{count++}
  \end{itemize}

  \medskip
  \begin{greenbox}{}
    Locking can guarantee both visibility and atomicity; \texttt{volatile} variables
    can only guarantee visibility
  \end{greenbox}
  
\end{frame}

\begin{frame}
\frametitle{The thread-safeness problem}

\begin{itemize}
\item thread-safe libraries are being developed nowadays
      \begin{itemize}
      \item their classes should be thread-safe
      \end{itemize}
\item but what does thread-safeness mean exactly?
      \begin{itemize}
      \item it can be used in a multithreaded way?
      \item any multithreaded execution can be rephrased as a sequential execution?
      \end{itemize}
\item checking that a class is thread-safe is undecidable
      \begin{itemize}
      \item immutable classes are thread-safe!
      \item use thread-safe classes from the Java standard library
      \item for more complex cases, there are heuristics
      \end{itemize}
\item annotations \texttt{@ThreadSafe} and \texttt{@Immutable} can be helpful
\end{itemize}

\end{frame}

\begin{frame}
  \frametitle{Synchronized collections in \texttt{java.util.}}

  \begin{itemize}
  \item \texttt{Vector}
  \item \texttt{Hashtable}
  \item\texttt{Collections.synchronizedCollection(collection)}
  \item\texttt{Collections.synchronizedSet(set)}
  \item\texttt{Collections.synchronizedSortedSet(sortedSet)}
  \item\texttt{Collections.synchronizedList(list)}
  \item\texttt{Collections.synchronizedMap(map)}
  \end{itemize}

  They encapsulate their state and synchronize every method.
  They commit to explicit support for client-side locking
  through their own intrinsic lock
\end{frame}

\begin{frame}[fragile]
  \frametitle{Compound actions are not atomic}

\begin{alltt}
\alert{@NotThreadSafe}
public class UnsafeVectorHelpers \{
  public static <T> T getLast(Vector<T> list) \{
    int lastIndex = list.size() - 1;  // \alert{read}
    return list.get(lastIndex);       // \alert{write}
  \}

  public static <T> void deleteLast(Vector<T> list) \{
    int lastIndex = list.size() - 1;  // \alert{read}
    list.remove(lastIndex);           // \alert{write}
  \}
\}
\end{alltt}

In a concurrent setting, this might throw an \texttt{ArrayIndexOutOfBoundsException}

\end{frame}

\begin{frame}[fragile]
  \frametitle{Make compound actions atomic with client-side locking}

{\small\begin{alltt}
\alert{@ThreadSafe}
public class SafeVectorHelpers \{
  public static <T> T getLast(Vector<T> list) \{
    \alert{synchronized (list)} \{
      int lastIndex = list.size() - 1;
      return list.get(lastIndex);
    \}
  \}

  public static <T> void deleteLast(Vector<T> list) \{
    \alert{synchronized (list)} \{
      int lastIndex = list.size() - 1;
      list.remove(lastIndex);
    \}
  \}
\}
\end{alltt}}

\end{frame}

\begin{frame}[fragile]
  \frametitle{Hidden compound actions in iteration}

Iteration is a hidden check-then-act operation:
  
\begin{alltt}
\alert{@NotThreadSafe}
for (E element: vector)          // \alert{check}
  System.out.println(element);   // \alert{then-act}
\end{alltt}

A way to fix it with client-side locking:

\begin{alltt}
\alert{@ThreadSafe}
\alert{synchronized (vector)} \{
  for (E element: vector)
    System.out.println(element);
\}
\end{alltt}

However, this solution prevents other threads from accessing the vector for the whole duration
of the iteration, which is undesirable

\end{frame}

\begin{frame}
  \frametitle{Concurrent collections}

  \begin{greenbox}{Synchronized collections: \texttt{Vector}, \texttt{Hashtable}, \texttt{synchronizedXXX}}
    Thread-safety achieved by serializing all accesses: \alert{poor concurrency}
  \end{greenbox}

  \bigskip
  \begin{greenbox}{Concurrent collections: \texttt{ConcurrentHashMap}, \texttt{CopyOnWriteArrayList}}
    Multiple threads can concurrently access the collection in a thread-safe way:
    \alert{high concurrency, slightly increased memory footprint}
  \end{greenbox}

\end{frame}

\begin{frame}
  \frametitle{\texttt{ConcurrentHashMap implements ConcurrentMap}}

  \texttt{ConcurrentHashMap} uses \alert{lock striping}:

  \begin{itemize}
  \item readers can access the map concurrently
  \item readers and writers can access the map concurrently
  \item a limited number of writers can modify the map concurrently
  \item iterators are not fail-fast: they yield a snapshot of the map at the time of their creation
    and never throw a \texttt{ConcurrentModificationException}
  \item there is no need to synchronize during iteration
  \item there is no support for client-side locking
  \item there are extra atomic check-then-act operations
    \begin{itemize}
    \item \texttt{V putIfAbsent(K key, V value)}
    \item \texttt{boolean remove(K key, V value)}
    \item \texttt{boolean replace(K key, V oldValue, V newValue)}
    \item \texttt{V replace(K key, V newValue)}
    \end{itemize}
  \end{itemize}
\end{frame}

\begin{frame}
\frametitle{Maps and concurrency}

\begin{center}
\includegraphics[width=12cm]{pictures/maps.jpg}
\end{center}

\end{frame}

\begin{frame}
\frametitle{Tools for concurrent software development}

\begin{itemize}
\item the standard \texttt{-Xprof} Java profiler is a basic tool for simple
      profiling: identifies blocking time and deadlocks
\item the YourKit Java profiler provides detailed information on block time
      and monitor usage and identifies deadlocks
\item static code analyzers might help (in the future)
\end{itemize}

\end{frame}

\begin{frame}
\frametitle{References}
\begin{greenbox}{}
\begin{center}
\includegraphics[width=3cm]{pictures/jcip.jpg}
\hspace*{1cm}
\includegraphics[width=3cm]{pictures/ac.jpg}
\hspace*{1cm}
\includegraphics[width=3cm]{pictures/android_threading.png}
\end{center}
\end{greenbox}

\end{frame}

\end{document}

