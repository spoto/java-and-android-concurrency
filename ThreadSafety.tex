\documentclass[11pt]{beamer}  %% versione proiettore
%%\documentclass[11pt,handout]{beamer} %% versione stampa
\usepackage{lucidiJb-2ed}

\mode<article>
{
  \usepackage{fullpage}
  \usepackage{hyperref}
}

\mode<presentation>
{
  \setbeamertemplate{background canvas}[vertical shading][bottom=red!10,top=blue!10]
  \usetheme{ThreadSafety}
  \usefonttheme[onlysmall]{structurebold}
}

\title{Thread Safety}
\author{Fausto Spoto}
\subtitle{Java and Android Concurrency}
\institute{Universit\`a di Verona, Italy}
\date{February 2017}

\setbeamercovered{invisible}

\begin{document}

\frame{\titlepage}

%\begin{center}
%\includegraphics[width=4cm]{pictures/multicore.jpg}
%\end{center}

\begin{frame}
\frametitle{Object State}

\begin{greenbox}{}
  An object \alert{state} is its data, stored in \alert{state variables} such as its
  instance fields. It might include the state of other, dependent objects. It encoppasses any data
  that can affect its externally visible behavior
\end{greenbox}

\begin{description}
  \item[shared state:] accessed by multiple threads
  \item[mutable state:] it could change during its lifetime
\end{description}

\begin{greenbox}{}
  Whenever more  than one thread accesses a given state variable,
  and one of them might write to it, they all must coordinate their access to it using synchronization
\end{greenbox}

\end{frame}

\begin{frame}
\frametitle{State Encapsulation}

\begin{greenbox}{}
  When designing thread-safe classes, good object-oriented techniques -- encapsulation,
  immutability, and clear specification of invariants -- are your best friends
\end{greenbox}

\medskip
\begin{greenbox}{Your enemies}
  \begin{itemize}
  \item public fields
  \item static fields
  \item mutability
  \item state leakage
  \end{itemize}
\end{greenbox}

\medskip
\begin{greenbox}{}
  It is far easier to design a class to be thread-safe than to retrofit it to thread-safety later
\end{greenbox}

\end{frame}

\begin{frame}
\frametitle{What is Thread Safety?}

\begin{center}
  \alert{Largely philosophical question}
\end{center}

\begin{greenbox}{}
  A class is \alert{correct} when it conforms to its specification.
  A class is \alert{thread-safe} when it continues to behave correctly
  when accessed from multiple threads, regardless of the scheduling
  of those threads, and with no additional synchronization
  or other coordination on the part of the calling code
\end{greenbox}

\medskip

\begin{greenbox}{}
  No set of operations -- calls to public methods of reads or writes of public fields -- performed
  sequentially or concurrently on instances of a thread-safe class can cause an instance to be
  in an invalid state
\end{greenbox}

\end{frame}

\begin{frame}
  \frametitle{Java Servlets}

  \begin{center}
    \includegraphics[width=7.5cm]{pictures/servlets.png}
  \end{center}

  Eclipse can create \alert{dynamic web projects} and export them into \alert{.war} files

\end{frame}

\begin{frame}[fragile]
  \frametitle{Heroku: \url{https://www.heroku.com}}
%In Eclipse, export servlets project in war format: servlets.war

\begin{greenbox}{Create account at \url{https://signup.heroku.com}. Then install}
  {\scriptsize
  \texttt{sudo add-apt-repository "deb https://cli-assets.heroku.com/branches/stable/apt ./"}\\
  \texttt{sudo apt install curl}\\
  \texttt{curl -L https://cli-assets.heroku.com/apt/release.key | sudo apt-key add -}\\
  \texttt{sudo apt-get update}\\
  \texttt{sudo apt-get install heroku}
  }
\end{greenbox}

\medskip
\begin{greenbox}{\texttt{heroku --version}}
  heroku-cli/5.6.27-7c0098a (linux-amd64) go1.7.5
\end{greenbox}

\medskip
\begin{greenbox}{\texttt{heroku login}}
Enter your Heroku credentials.\\
Email: \alert{fausto.spoto@univr.it}\\
Password (typing will be hidden): \alert{verysafepassword}\\
Authentication successful.
\end{greenbox}

\end{frame}

\begin{frame}[fragile]
  \frametitle{Heroku: Create and Deploy Application}

\begin{greenbox}{\texttt{heroku create}}
Creating app... done, \alert{limitless-bayou-56277}\\
https://limitless-bayou-56277.herokuapp.com/
https://git.heroku.com/limitless-bayou-56277.git
\end{greenbox}

\medskip
\begin{greenbox}{Install the command line deployment plugin}
  \texttt{heroku plugins:install heroku-cli-deploy}
\end{greenbox}

\medskip
\begin{greenbox}{Deploy the application in Heroku}
  \texttt{heroku war:deploy servlets.war --app \alert{limitless-bayou-56277}}
\end{greenbox}

\begin{center}
With a browser, go to
\url{https://limitless-bayou-56277.herokuapp.com/StatelessFactorizer?number=250}
\end{center}

\end{frame}

\begin{frame}[fragile]
  \frametitle{A Thread-Safe Factorizing Servlet}

  \vspace*{-1ex}
{\scriptsize\begin{alltt}
@ThreadSafe
\alert{@WebServlet("/StatelessFactorizer")} // publication path
public class StatelessFactorizer extends \alert{HttpServlet} \{

  @Override
  protected void \alert{doGet}(HttpServletRequest request, HttpServletResponse response) \{
    doPost(request, response); // delegation
  \}

  @Override
  protected void \alert{doPost}(HttpServletRequest request, HttpServletResponse response) \{
    BigInteger number = extractFromRequest(request);
    BigInteger[] factors = factor(number);  // see implementation in Eclipse
    encodeIntoResponse(response, factors);
  \}

  protected BigInteger extractFromRequest(HttpServletRequest request) \{
    return new BigInteger(request.\alert{getParameter}("number"));
  \}

  protected void encodeIntoResponse(HttpServletResponse response, BigInteger[] fs) \{
    response.\alert{getOutputStream}().println(Arrays.toString(fs));
  \}
\}
\end{alltt}}
\end{frame}

\begin{frame}
  \frametitle{Stateless Objects}

  \begin{greenbox}{There is only an instance of the servlet object}
    The servlet container receives many concurrent requests
    but creates a single instance of
    \texttt{it.univr.servlets.StatelessFactorizer}. All requests are
    routed to that instance, each running inside its own thread!
    There is no problem in this example, since the servlet keeps no state
    information in its fields
  \end{greenbox}

  \medskip
  \begin{greenbox}{}
    \begin{center}
      Stateless objects are always thread-safe
    \end{center}
  \end{greenbox}

\end{frame}

\begin{frame}[fragile]
  \frametitle{A Program that Connects to the Servlet}

  Before seeing other servlets, let us see how a client can connect to the
  \texttt{StatelessFactorizer} and ask its service:

{\scriptsize\begin{alltt}
public class ServletClient \{
  public final static String SERVER
    = "https://limitless-bayou-56277.herokuapp.com/StatelessFactorizer?number=250";

  public static void main(String[] args)
      throws MalformedURLException, IOException \{

    URL url = new URL(SERVER);
    URLConnection conn = url.openConnection();

    try (BufferedReader in = new BufferedReader(new InputStreamReader
        (conn.getInputStream()))) \{

      String response;
      while ((response = in.readLine()) != null)
        System.out.println(response);
    \}
  \}
\}
\end{alltt}}

\end{frame}

\begin{frame}[fragile]
  \frametitle{Let us Count the Number of Requests}

{\small\begin{alltt}
\alert{@NotThreadSafe}
@WebServlet("/UnsafeCountingFactorizer")
public class UnsafeCountingFactorizer \alert{extends StatelessFactorizer} \{
  private \alert{long count = 0};

  public long getCount() \{
    return \alert{count};
  \}

  @Override
  protected void doPost(HttpServletRequest request,
                        HttpServletResponse response) \{
    BigInteger number = extractFromRequest(request);
    BigInteger[] factors = factor(number);
    \alert{++count};
    encodeIntoResponse(response, factors);
  \}
\}
\end{alltt}}
\end{frame}

\begin{frame}[fragile]
  \frametitle{Non-Atomic Operations}

  \begin{greenbox}{}
    The addition of just a bit of shared, mutable state makes
    the servlet non-thread-safe, because of a non-atomic operation
    \texttt{++count}. It gets compiled into many Java bytecode instructions:
\begin{alltt}
aload 0
aload 0
getfield count:L       // \alert{read}
const 1L
ladd                   // \alert{modify}
putfield count:L       // \alert{write}
\end{alltt}
  \end{greenbox}

  \medskip
  \begin{greenbox}{Beware of}
    \begin{itemize}
    \item \alert{read-modify-write} operations on shared, mutable state
    \item \alert{check-then-act} sequences on shared, mutable state
    \end{itemize}
  \end{greenbox}

\end{frame}

\begin{frame}[fragile]
  \frametitle{Race Condition}

  \begin{greenbox}{}
    A race condition occurs when the correctness of a computation depends on the
    relative timing or interleaving of multiple threads by the runtime; in other words,
    when getting the right answer relies on lucky timing
  \end{greenbox}

  \bigskip
  A typical example of race condition is in the initialization of
  shared, meant to be unique instances of objects:

{\small\begin{alltt}
\alert{@NotThreadSafe}
public class LazyInitRace \{
  private ExpensiveObject instance = null;

  public ExpensiveObject getInstance() \{
    if (instance == null) // \alert{check}
      instance = new ExpensiveObject(); // \alert{then act}

    return instance;
  \}
\}
\end{alltt}}
  
\end{frame}

\begin{frame}[fragile]
  \frametitle{Atomicity}

  \begin{greenbox}{}
    Operations $A$ and $B$ are \alert{atomic} with respect to each other if,
    from the perspective of a thread executing $A$, when another thread executes $B$,
    either all of $B$ has executed ot none of it has. An \alert{atomic operation}
    is one that is atomic with respect to all operations, including itself, that operate on
    the same state
  \end{greenbox}

{\scriptsize\begin{alltt}
\alert{@ThreadSafe} @WebServlet("/CountingFactorizer")
public class CountingFactorizer extends StatelessFactorizer \{
  private final \alert{AtomicLong count = new AtomicLong(0L)};

  public long getCount() \{ return \alert{count.get()}; \}

  @Override
  protected void doPost(HttpServletRequest request, HttpServletResponse response) \{
    BigInteger number = extractFromRequest(request);
    BigInteger[] factors = factor(number);
    \alert{count.incrementAndGet()};
    encodeIntoResponse(response, factors);
  \}
\}
\end{alltt}}
    
\end{frame}

\begin{frame}[fragile]
  \frametitle{Let us Cache the Last Request Result}

{\scriptsize\begin{alltt}
\alert{@NotThreadSafe}
@WebServlet("/UnsafeCachingFactorizer")
public class UnsafeCachingFactorizer extends StatelessFactorizer \{
  private final \alert{AtomicReference<BigInteger> lastNumber = new AtomicReference<>()};
  private final \alert{AtomicReference<BigInteger[]> lastFactors = new AtomicReference<>()};

  @Override
  protected void doPost(HttpServletRequest request, HttpServletResponse response) \{
    BigInteger number = extractFromRequest(request);
    if (number.equals(\alert{lastNumber.get()}))
      encodeIntoResponse(response, \alert{lastFactors.get()});
    else \{
      BigInteger[] factors = factor(number);
      \alert{lastNumber.set(number)};
      \alert{lastFactors.set(factors)};
      encodeIntoResponse(response, factors);
    \}
  \}
\}
\end{alltt}}
\end{frame}

\begin{frame}
  \frametitle{From One State Variable to Two State Variables}

  Since a single state variable of type \text{long} can be used thread-safely
  by translating it into an \texttt{AtomicLong}, we could have expected to
  do the same with two state variables of reference type, by using \texttt{AtomicReference}

  \bigskip
  \begin{greenbox}{The result is not thread-safe!}
    There is an implicit link between the values of the two (thread-safe)
    state variables. Hence, to preserve state consistency, such related state
    variables must be updated in a single atomic operation
  \end{greenbox}
  
\end{frame}

\begin{frame}[fragile]
  \frametitle{Recovering Thread-Safeness through Synchronization}

  In order to make updates to the two state variables atomic, they must be embedded
  inside the same \texttt{synchronized} block, hence exploiting the \alert{mutex nature of
  Java's intrinsic locks}

{\scriptsize\begin{alltt}
\alert{@ThreadSafe} @WebServlet("/SynchronizedFactorizer")
public class SynchronizedFactorizer extends StatelessFactorizer \{
  private \alert{@GuardedBy("this")} BigInteger lastNumber;
  private \alert{@GuardedBy("this")} BigInteger[] lastFactors;

  @Override protected \alert{synchronized} void doPost(...) \{
    BigInteger number = extractFromRequest(request);
    if (number.equals(lastNumber)) encodeIntoResponse(response, lastFactors);
    else \{
      BigInteger[] factors = factor(number); lastNumber = number;
      lastFactors = factors; encodeIntoResponse(response, factors);
    \}
  \}
\}
\end{alltt}}

  We have recovered thread-safety at the price of efficiency: only one
  \texttt{SynchronizedFactorizer} can run at a time. This is not what concurrency was meant for
    
\end{frame}

\begin{frame}[fragile]
  \frametitle{Reentrancy: A Natural Choice for an OO Language}

  \begin{greenbox}{}
    Java's intrinsic locks are \alert{reentrant}, that is,
    if a thread tries to acquire alock that \alert{it} already holds,
    the request succeeds
  \end{greenbox}

  \medskip
  This is necessary in an object-oriented language, or otherwise overriding
  of synchronized methods would deadlock:

  \medskip
\begin{verbatim}
public class Widget {
  public synchronized void doSomething() { ... }
}

public class LoggingWidget extends Widget {
  public synchronized void doSomething() {
    System.out.println(toString() + ": calling doSomething");
    super.doSomething();
  }
}  
\end{verbatim}

\end{frame}

\begin{frame}[fragile]
  \frametitle{Guarding State with a Lock}

  \begin{greenbox}{}
    For each mutable state variable that may be accessed by more than one
    thread, \alert{all} accesses to that variables (both for
    writing \alert{and for reading}) must be performed
    with the \alert{same} lock held. In that case, we say that the
    variable is \alert{guarded by} that lock
  \end{greenbox}

  \medskip
  \begin{greenbox}{}
    For every invariant that involves more than one variable, \alert{all} the variables
    involved in that invariant must be guarded by the \alert{same} lock
  \end{greenbox}
  
  \medskip
  \begin{greenbox}{}
    Make clear to maintainers which lock is used to access a shared, mutable variable.
    This is the goal of the \texttt{@GuardedBy} annotation
  \end{greenbox}

  \medskip
  Can we save the world by making everything \texttt{synchronized} ? Not really\ldots

  \begin{verbatim}
    if (!vector.contains(element))
      vector.add(element)
  \end{verbatim}
  
\end{frame}

\begin{frame}
  \frametitle{Performance}

  Making the whole servlet \texttt{doPost()} method \texttt{synchronized}
  restored thread-safety at the price of \alert{performance}: only a thread
  can execute at a time. Let us try to put inside \texttt{synchronized} blocks
  only those portions of code that really need synchronization

  \bigskip
  \begin{greenbox}{}
    Avoid holding locks during lengthy computations or operations at risk of not
    completing quickly such as network or console I/O (or sleep!)
  \end{greenbox}
  
\end{frame}

\begin{frame}[fragile]
  \frametitle{A Factorizer that Keeps a Cache, with Good Performance}

{\small\begin{alltt}
\alert{@ThreadSafe}
@WebServlet("/CachedFactorizer")    
public class CachedFactorizer extends StatelessFactorizer \{
  private \alert{@GuardedBy("this")} BigInteger lastNumber;
  private \alert{@GuardedBy("this")} BigInteger[] lastFactors;
  private \alert{@GuardedBy("this")} long hits;
  private \alert{@GuardedBy("this")} long cacheHits;

  public \alert{synchronized} long getHits() \{
    return hits;
  \}

  public \alert{synchronized} double getCacheHitsRatio() \{
    return cacheHits / (double) hits;
  \}
\end{alltt}}

\end{frame}

\begin{frame}[fragile]
  \frametitle{A Factorizer that Keeps a Cache, with Good Performance}

\vspace*{-1ex}
{\scriptsize\begin{alltt}
  @Override protected void doPost(...) \{
    BigInteger number = extractFromRequest(request);
    BigInteger[] factors = null;

    \alert{synchronized (this) \{}
      ++hits;
      if (number.equals(lastNumber)) \{
        ++cacheHits;
        factors = lastFactors;
      \}
    \alert{\}}

    if (factors == null) \{
      factors = factor(number); // \alert{long operation: outside synchronization!}

      \alert{synchronized (this) \{}
        lastNumber = number;
        lastFactors = factors;
      \alert{\}}
    \}

    encodeIntoResponse(response, factors);
  \}
\}
\end{alltt}}

\end{frame}

\begin{frame}[fragile]
  \frametitle{Exercise 1}

  Write a web application implementing a chat server, with two servlets:

  \bigskip
  \begin{greenbox}{Add a message to the chat}
    \verb!AddMessage?author=AAAA&text=TTTT!
  \end{greenbox}

  \bigskip
  \begin{greenbox}{List the last messages of the chat}
    \verb!ListMessages?howmany=HHHH!

    If there are fewer messages, only lists those available.
    The list is provided in the output of the servlet, in increasing
    chronological order, as a sequence of XML messages:

{\scriptsize\begin{verbatim}
<message>
  <author>
    Fausto
  </author>
  <text>
    Hello, are you listening?
  </text>
</message>  
\end{verbatim}}
  \end{greenbox}
  
\end{frame}

\begin{frame}
  \frametitle{Exercise 1: Suggestion}

  Servlets have a \alert{context}, holding data that must be made available to
  the whole application, that is, to all servlets of the same web application.
  This context can be accessed by writing:

  \begin{center}\texttt{ServletContext context = getServletContext()}\end{center}

  Data can be stored and retrieved from the context by using its methods:

  \begin{itemize}
  \item \texttt{setAttribute(String key, Object value)}
  \item \texttt{Object getAttribute(String key)}, which yields \texttt{null} if the attribute is unknown
  \end{itemize}

  Since attribute values are shared across all instances of all servlets, \alert{they must be thread-safe}

  In the exercise, the list of chat messages might be an attribute

\end{frame}

\begin{frame}[fragile]
  \frametitle{Exercise 2}

  Write a command-line client to the chat web application, that allows one to
  post new messages to the chat, by specifying author and text, and to
  list the last (up to) 10 messages in the chat, in increasing chronological order, such as:

\begin{verbatim}
    Fausto says:
    Hello, are you listening?

    Linda says:
    Kind of, I'm busy writing servlets

    Fausto says:
    Wow, you are a Java expert!
\end{verbatim}

  Share the same chat server across many clients and try its concurrent use
\end{frame}

\end{document}
