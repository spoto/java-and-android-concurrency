\documentclass[11pt]{beamer}  %% versione proiettore
%%\documentclass[11pt,handout]{beamer} %% versione stampa
\usepackage{lucidiJb-2ed}

\mode<article>
{
  \usepackage{fullpage}
  \usepackage{hyperref}
}

\mode<presentation>
{
  \setbeamertemplate{background canvas}[vertical shading][bottom=red!10,top=blue!10]
  \usetheme{ComposingObjects}
  \usefonttheme[onlysmall]{structurebold}
}

\title{Composing Objects}
\author{Fausto Spoto}
\subtitle{Java and Android Concurrency}
\institute{Universit\`a di Verona, Italy}
\date{March 2017}

\setbeamercovered{invisible}

\begin{document}

\frame{\titlepage}

\begin{frame}
  \frametitle{Three Questions}

  This section will answer three questions:

  \begin{enumerate}
  \item is a thread-safe class necessarily built from thread-safe components?
  \item if a class is composed from thread-safe components, is it thread-safe as well?
  \item if a class is thread-safe, is a subclass still thread-safe?
  \end{enumerate}

  The answer will constantly be ``\alert{not always, it depends}''\ldots
\end{frame}

\begin{frame}
  \frametitle{Designing a Thread-Safe Class}

  \begin{itemize}
  \item identify the variables that form the object's state
    \begin{itemize}
    \item a subset of everything reachable from its fields
      (that is, the objects it \alert{owns}, think about collection classes)
    \end{itemize}
  \item identify the invariants that constrain the state variables
    \begin{itemize}
    \item not all values might be valid
    \item there might be constraints between state variables
    \item operations might have preconditions
    \end{itemize}
  \item establish a policy for managing concurrent access to the object's state
  \end{itemize}
  
\end{frame}

\begin{frame}[fragile]
  \frametitle{Instance Confinement}

  One can use a non-thread-safe instance field in a thread-safe class, as long
  as it is properly encapsulated and accessed through a suitable
  synchronization policy. The following example uses the \alert{monitor pattern}:

{\scriptsize\begin{alltt}
\alert{@ThreadSafe}
public class PersonSet \{
  \alert{@GuardedBy("this")} private final Set<Person> mySet = new HashSet<>();

  public \alert{synchronized} void addPerson(Person p) \{
    mySet.add(p);
  \}

  public \alert{synchronized} boolean containsPerson(Person p) \{
    return mySet.contains(p);
  \}

  interface Person \{\}
\}
\end{alltt}}
  
\end{frame}

\begin{frame}[fragile]
  \frametitle{Lock Encapsulation}

  An alternative implementation might use a private lock, to avoid interference from
  outside the class:

  {\scriptsize\begin{alltt}
\alert{@ThreadSafe}
public class PersonSet \{
  \alert{@GuardedBy("itself")} private final Set<Person> mySet = new HashSet<>();

  public void addPerson(Person p) \{
    \alert{synchronized (mySet)} \{
      mySet.add(p);
    \}
  \}

  public boolean containsPerson(Person p) \{
    \alert{synchronized (mySet)} \{
      return mySet.contains(p);
    \}
  \}

  interface Person \{\}
\}
\end{alltt}}
\end{frame}

\begin{frame}[fragile]
  \frametitle{Instance Confinement through Defensive Copy}

  We will use this non-thread-safe class and still get a thread-safe class:

  \begin{alltt}
\alert{@NotThreadSafe}
public class MutablePoint \{
  public int x, y;

  public MutablePoint() \{
    x = 0;
    y = 0;
  \}

  public MutablePoint(MutablePoint p) \{
    this.x = p.x;
    this.y = p.y;
  \}
\}
  \end{alltt}
  
\end{frame}

\begin{frame}[fragile]
  \frametitle{Instance Confinement through Defensive Copy}

  A class tracking the current positions of a set of vehicles:

  \begin{alltt}
\alert{@ThreadSafe}
public class MonitorVehicleTracker \{
  \alert{@GuardedBy("this")} private final
    Map<String, MutablePoint> locations;

  private static Map<String, MutablePoint> deepCopy
      (Map<String, MutablePoint> m) \{

    Map<String, MutablePoint> result = new HashMap<>();
    for (String id : m.keySet())
      result.put(id, new MutablePoint(m.get(id)));

    return Collections.unmodifiableMap(result);
  \}
  \end{alltt}
  
\end{frame}

\begin{frame}[fragile]
  \frametitle{Instance Confinement through Defensive Copy}

{\scriptsize\begin{alltt}
  public MonitorVehicleTracker(Map<String, MutablePoint> locations) \{
    this.locations = \alert{deepCopy}(locations);
  \}

  public \alert{synchronized} Map<String, MutablePoint> getLocations() \{
    return \alert{deepCopy}(locations);
  \}

  public \alert{synchronized} MutablePoint getLocation(String id) \{
    MutablePoint loc = locations.get(id);
    return loc == null ? null : \alert{new MutablePoint}(loc);
  \}

  public \alert{synchronized} void setLocation(String id, int x, int y) \{
    MutablePoint loc = locations.get(id);
    if (loc == null)
      throw new IllegalArgumentException("No such ID: " + id);
    loc.x = x;
    loc.y = y;
  \}
\}
\end{alltt}}
  
\end{frame}

\begin{frame}
  \frametitle{Delegating Thread-Safety}

  If a class has only thread-safe instance fields, is it thread-safe as well?
  \begin{greenbox}{}
    \begin{center}
      it depends\ldots
    \end{center}
  \end{greenbox}

  \texttt{CountingFactorizer} was thread-safe because its state coincides with that
  of its \texttt{AtomicLong} thread-safe only instance field and since it does not
  impose any extra constraint on the states of that class

\end{frame}

\begin{frame}[fragile]
  \frametitle{An Example of Delegation of Thread-Safety}

  Let us delegate the thread-safety of our vehicle tracker to the thread-safety
  of the library class \texttt{ConcurrentHashMap}. To further simplify the work, let us
  use an immutable point class:

\begin{alltt}
\alert{@Immutable}
public class Point \{
  public final int x, y;

  public Point(int x, int y) \{
    this.x = x;
    this.y = y;
  \}
\}
\end{alltt}

\end{frame}

\begin{frame}[fragile]
  \frametitle{An Example of Delegation of Thread-Safety}

No explicit synchronization!

{\scriptsize\begin{alltt}
\alert{@ThreadSafe}
public class DelegatingVehicleTracker \{
  private final \alert{ConcurrentMap}<String, Point> locations;

  public DelegatingVehicleTracker(Map<String, Point> points) \{
    locations = new \alert{ConcurrentHashMap}<>(points);
  \}

  public Map<String, Point> getLocations() \{
    return Collections.unmodifiableMap(new HashMap<>(locations));
  \}

  public Point getLocation(String id) \{
    return locations.get(id);
  \}

  public void setLocation(String id, int x, int y) \{
    if (locations.\alert{replace}(id, new Point(x, y)) == null)
      throw new IllegalArgumentException("invalid vehicle name: " + id);
  \}
\}
\end{alltt}}

\end{frame}

\begin{frame}[fragile]
  \frametitle{Delegating to More, \alert{Independent} State Variables}

  The resulting class is thread-safe if the state variables are
  thread-safe and independent:

{\scriptsize\begin{alltt}
\alert{@ThreadSafe}    
public class VisualComponent \{
  private final List<KeyListener> keyListeners = new \alert{CopyOnWriteArrayList}<>();
  private final List<MouseListener> mouseListeners = new \alert{CopyOnWriteArrayList}<>();

  public void addKeyListener(KeyListener listener) \{
    keyListeners.add(listener);
  \}

  public void addMouseListener(MouseListener listener) \{
    mouseListeners.add(listener);
  \}

  public void removeKeyListener(KeyListener listener) \{
    keyListeners.remove(listener);
  \}

  public void removeMouseListener(MouseListener listener) \{
    mouseListeners.remove(listener);
  \}
\end{alltt}}

\end{frame}

\begin{frame}[fragile]
  \frametitle{Delegating to More, \alert{Independent} State Variables}

\begin{alltt}
  public void fireKeyListeners(KeyEvent event) \{
    for (KeyListener listener: keyListeners)
      listener.keyPressed(event);
  \}

  public void fireMouseListeners(MouseEvent event) \{
    for (MouseListener listener: mouseListeners)
      listener.mouseClicked(event);
  \}
\}
\end{alltt}

\begin{greenbox}{\texttt{CopyOnWriteArrayList}}
  This thread-safe library class allows iterating on the list and its concurrent update,
  without throwing a \texttt{ConcurrentModificationException}. This is payed with an increased
  cost for list update
\end{greenbox}

\end{frame}

\begin{frame}[fragile]
  \frametitle{Invariants between State Variables Make Delegation Fail}

{\scriptsize\begin{alltt}
\alert{@NotThreadSafe}
public class NumberRange \{
  // \alert{INVARIANT: lower <= upper}
  private final AtomicInteger lower = new AtomicInteger(0);
  private final AtomicInteger upper = new AtomicInteger(0);

  public void setLower(int i) \{
    if (i > upper.get())  // \alert{check}
      throw new IllegalArgumentException("can't set lower to " + i + " > upper");
    lower.set(i);         // \alert{then act}
  \}

  public void setUpper(int i) \{
    if (i < lower.get())  // \alert{check}
      throw new IllegalArgumentException("can't set upper to " + i + " < lower");
    upper.set(i);         // \alert{then act}
  \}

  public boolean isInRange(int i) \{
    return (i >= lower.get() && i <= upper.get());
  \}
\}    
\end{alltt}}
\end{frame}

\begin{frame}[fragile]
  \frametitle{Invariants between State Variables Require Synchronization}

{\scriptsize\begin{alltt}
\alert{@ThreadSafe}
public class NumberRangeSynchronized \{
  // INVARIANT: lower <= upper
  private \alert{@GuardedBy("this") int} lower = 0;
  private \alert{@GuardedBy("this") int} upper = 0;

  public \alert{synchronized} void setLower(int i) \{
    if (i > upper)
      throw new IllegalArgumentException("can't set lower to " + i + " > upper");
    lower = i;
  \}

  public \alert{synchronized} void setUpper(int i) \{
    if (i < lower)
      throw new IllegalArgumentException("can't set upper to " + i + " < lower");
    upper = i;
  \}

  public \alert{synchronized} boolean isInRange(int i) \{
    return i >= lower && i <= upper;
  \}
\}
\end{alltt}}
\end{frame}

\begin{frame}[fragile]
  \frametitle{Extending Thread-Safe Classes}

  Suppose we want to add an atomic \texttt{putIfAbsent} operation to the
  thread-safe library class \texttt{Vector}. This is a check-then-act operation,
  hence synchronization is needed, despite the fact that each vector operation
  is already synchronized

{\small\begin{alltt}
\alert{@ThreadSafe}
public class BetterVector<E> extends Vector<E> \{
  public \alert{synchronized} boolean putIfAbsent(E x) \{
    boolean absent = !contains(x);
    if (absent)
      add(x);
    return absent;
  \}
\}
\end{alltt}}

\begin{greenbox}{}
  This works since \texttt{Vector} is explicitly designed to use
  its intrinsic lock for synchronization. Otherwise, changing
  the synchronization policy in \texttt{Vector} would break
  \texttt{BetterVector}
\end{greenbox}

\end{frame}

\begin{frame}[fragile]
  \frametitle{Client-Side Locking}

  \texttt{BetterVector} uses \alert{client-side locking}, that is,
  locks \alert{the same lock} that the base class uses for its own synchronization.

  Client-side locking can also be used for composition:

{\scriptsize\begin{alltt}
\alert{@ThreadSafe}
public class ListHelper<E> \{
  public final List<E> list = Collections.synchronizedList(new ArrayList<E>());

  public boolean putIfAbsent(E x) \{
    \alert{synchronized (list)} \{
      boolean absent = !list.contains(x);
      if (absent)
        list.add(x);
      return absent;
    \}
  \}
\}
\end{alltt}}

Using \texttt{synchronized (this)} instead would make the class non-thread-safe

\end{frame}

\begin{frame}[fragile]
  \frametitle{Adding Another Layer of Synchronization}

  Another, less fragile, way of adding a \texttt{putIfAbsent} operation to
  a synchronized list:

{\scriptsize\begin{alltt}
\alert{@ThreadSafe}
public class ImprovedList<T> implements List<T> \{
  private final List<T> list;

  // \alert{PRE: list argument is thread-safe}
  public ImprovedList(List<T> list) \{ this.list = list; \}

  public \alert{synchronized} boolean putIfAbsent(T x) \{
    boolean absent = !list.contains(x);
    if (absent)
      list.add(x);
    return absent;
  \}

  \alert{// Plain vanilla delegation for List methods}
  \alert{// Mutative methods must be synchronized to ensure atomicity of putIfAbsent}
  public int size() \{ return list.size(); \}
  public \alert{synchronized} boolean add(T e) \{ return list.add(e); \}
  ...
\}
\end{alltt}}
  
\end{frame}

\begin{frame}
  \frametitle{Documenting Synchronization Policies}

  \begin{itemize}
  \item  Client-side locking shows that it is important to know the synchronization policy
    used by a class, in order to extend the class with new synchronized functionalities
  \item  \alert{Sadly, current programming languages have no syntactical support for specifying and
      enforcing a synchronization policy}
  \end{itemize}

  \bigskip
  \begin{greenbox}{It remains documentation}
    Document a class's thread safety guarantees for its clients; document its synchronization
    policy for its maintainers

    \begin{itemize}
    \item use annotations \texttt{@ThreadSafe}, \texttt{@NotThreadSafe}, \texttt{@GuardedBy},
      \texttt{@Immutable}
    \item state if client-side locking is supported, and which lock should be used
    \end{itemize}
  \end{greenbox}

\end{frame}

\begin{frame}[fragile]
  \frametitle{Exercize}

  Write a web application with two servlets:
  \begin{itemize}
  \item \texttt{NextPrime} prints the next prime number: the first invocation prints $2$,
    the second $3$, the third $5$ and so on
  \item \texttt{NextPrimes?howmany=NNNN} prints the next \alert{consecutive}
    \texttt{NNNN} prime numbers, such as
    \verb![53, 59, 61, 67, 71, 73, 79, 83, 89, 97]!
  \end{itemize}

  These two servlets must use the same next prime counter: if \texttt{NextPrime} prints
  $7$, then \texttt{NextPrimes} will start from $11$

\end{frame}

\end{document}
