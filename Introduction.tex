\documentclass[11pt]{beamer}  %% versione proiettore
%%\documentclass[11pt,handout]{beamer} %% versione stampa
\usepackage{lucidiJb-2ed}

\mode<article>
{
  \usepackage{fullpage}
  \usepackage{hyperref}
}

\mode<presentation>
{
  \setbeamertemplate{background canvas}[vertical shading][bottom=red!10,top=blue!10]
  \usetheme{Introduction}
  \usefonttheme[onlysmall]{structurebold}
}

\title{Introduction}
\author{Fausto Spoto}
\subtitle{Java and Android Concurrency}
\institute{Universit\`a di Verona, Italy}
\date{February 2017}

\setbeamercovered{invisible}

\begin{document}

\frame{\titlepage}

\begin{frame}
\frametitle{Why Concurrency Matters}
\begin{itemize}
\item on monocore architectures, it allows one to
      keep the processor busy
      \begin{itemize}
      \item hence its use in all operating systems
      \item and its presence in programming languages: C, Java etc
      \end{itemize}
\item on multicore architectures, it also allows one
      to use all computing cores to solve a single problem
      \begin{itemize}
      \item getting important nowadays
      \end{itemize}
\end{itemize}

\begin{center}
\includegraphics[width=4cm]{pictures/multicore.jpg}
\hspace*{1cm}
\includegraphics[width=4cm]{pictures/shared_resource.jpg}
\end{center}

\end{frame}

\begin{frame}
\frametitle{Why Concurrency Matters More and More}

\begin{greenbox}{Modern frameworks are inherently multithreaded}
  Servlet containers run each request in a separate thread.
  You might be writing concurrent programs without even
  creating a single thread in your code!
\end{greenbox}

\bigskip
\begin{greenbox}{Mobile devices must be responsive}
\begin{itemize}
\item code in the user interface thread must terminate quickly,
      or otherwise the user experience gets degraded: long running
      tasks must be delegated to non-user interface threads
\item while a task is running, the user might need to
      start another app, make a call, surf the internet etc.
      The running task must continue in the background
\end{itemize}
\end{greenbox}

\end{frame}

\begin{frame}
\frametitle{Why Concurrency is Difficult}
\begin{itemize}
\item protocols might be wrong
      \begin{itemize}
      \item race conditions
      \item deadlocks
      \end{itemize}
\item protocols might be inefficient
      \begin{itemize}
      \item livelocks
      \item slower than monothreaded!
      \end{itemize}
\item data can be shared
\item data can be modified
\item data can be cached on each single core
      \begin{itemize}
      \item each core might have a different view of the memory
      \end{itemize}
\end{itemize}

\begin{redbox}{}
  \begin{center}
    Programmers are not enough qualified to deal with concurrency
  \end{center}
\end{redbox}

\end{frame}

\begin{frame}
\frametitle{Concurrency in Java}
\begin{itemize}
\item from Java 1.0:
      \begin{itemize}
      \item \emph{multithreading}: a thread is a process with shared heap
      \item extends \texttt{java.lang.Thread} and overrides \texttt{run}
      \item relatively slow to start
      \item \texttt{synchronized} blocks and methods
      \item each object has a lock accessible through
            \texttt{wait}/\texttt{notify}/\texttt{notifyAll}
      \item \texttt{volatile} variables are never kept into caches
      \item a formal Java memory model
      \end{itemize}
\item from Java 1.5:
      \begin{itemize}
      \item improved memory model
      \item \texttt{java.lang.concurrent.*} has many clever classes
            for concurrency:
            concurrent hashmaps, latches, futures, callables
      \item executors recycle threads to avoid their startup cost
      \end{itemize}
\item from Java 1.7:
      \begin{itemize}
      \item fork executors share tasks across threads for divide and conquer
      \end{itemize}
\item from Java 1.8:
  \begin{itemize}
    \item parallel collections with lambda expression tasks
  \end{itemize}
\end{itemize}
\end{frame}

\begin{frame}[fragile]
\frametitle{Task Executor and Task Specification}

\begin{center}
  \includegraphics[width=3.6cm]{pictures/runnable_thread.png}
\end{center}

\end{frame}

\begin{frame}[fragile]
  \frametitle{Creation of a Thread}

  A thread can be created by subclassing the \texttt{java.lang.Thread} class:
\begin{verbatim}
 public class MyThread extends Thread {
   @Override
   public void run() { ... do something here ... }
 }
 ...
 new MyThread().start();
\end{verbatim}

A thread can also be created by specifying the task in the constructor
of a new \texttt{java.lang.Thread}:
\begin{verbatim}
 public class MyRunnable implements Runnable {
   @Override
   public void run() { ... do something here ... }
 }
 ...
 new Thread(new MyRunnable()).start();
\end{verbatim}

\end{frame}

\begin{frame}[fragile]
\frametitle{The Meaning of \texttt{synchronized}}

The compiler translates

\begin{greenbox}{}
\begin{verbatim}
synchronized (expression) {
  body
}
\end{verbatim}
\end{greenbox}

into
\vspace*{1ex}

\begin{greenbox}{}
\begin{verbatim}
final temp = expression;
get the lock of temp
body
release the lock of temp
\end{verbatim}
\end{greenbox}

\vspace*{1ex}

\begin{redbox}{}
\texttt{temp} is constant hence lock and unlock are paired
\end{redbox}

\vspace*{1ex}

\begin{redbox}{}
Java's intrinsic locks are reentrant
\end{redbox}


\end{frame}

\begin{frame}[fragile]
\frametitle{The Meaning of a \texttt{synchronized} Instance Method}

The compiler translates

\begin{greenbox}{}
\begin{verbatim}
synchronized T foo(pars) { body }
\end{verbatim}
\end{greenbox}

into
\vspace*{1ex}

\begin{greenbox}{}
\begin{verbatim}
T foo(pars) {
  synchronized (this) { body }
}
\end{verbatim}
\end{greenbox}

that is into
\vspace*{1ex}

\begin{greenbox}{}
\begin{verbatim}
T foo(pars) {
  get the lock of this
  body
  release the lock of this
}
\end{verbatim}
\end{greenbox}

\vspace*{1ex}

\begin{redbox}{}
``\texttt{this}'' is constant in Java hence lock and unlock are paired
\end{redbox}

\end{frame}

\begin{frame}[fragile]
\frametitle{The Meaning of a \texttt{synchronized} Static Method of Class \texttt{C}}

The compiler translates

\begin{greenbox}{}
\begin{verbatim}
synchronized static T foo(pars) { body }
\end{verbatim}
\end{greenbox}

into
\vspace*{1ex}

\begin{greenbox}{}
\begin{verbatim}
static T foo(pars) {
  synchronized (C.class) { body }
}
\end{verbatim}
\end{greenbox}

that is into
\vspace*{1ex}

\begin{greenbox}{}
\begin{verbatim}
T foo(pars) {
  get the lock of C.class
  body
  release the lock of C.class
}
\end{verbatim}
\end{greenbox}

\vspace*{1ex}

\begin{redbox}{}
\texttt{C.class} is constant in Java hence lock and unlock are paired
\end{redbox}

\end{frame}

\begin{frame}
\frametitle{The Meaning of \texttt{wait}/\texttt{notify}/\texttt{notifyAll} (Historical!)}

\begin{greenbox}{\texttt{expression.wait()}}
  Waits until somebody will notify on the value of \texttt{expression},
  temporarily releasing any lock held by the current thread
\end{greenbox}

\vspace*{2ex}

\begin{greenbox}{\texttt{expression.notify()}}
Wakes up a thread waiting on the value of \texttt{expression},
if any. If more threads are waiting, one of them
is non-deterministically chosen and awakened
\end{greenbox}

\vspace*{2ex}

\begin{greenbox}{\texttt{expression.notifyAll()}}
Wakes up all threads waiting on the value of \texttt{expression}, if any.
The awakened threads must recheck the condition they were waiting for
\end{greenbox}

\vspace*{2ex}

\begin{redbox}{}
These calls must occur only when the thread has already synchronized on the value
of \texttt{expression}
\end{redbox}

\end{frame}

\begin{frame}
\frametitle{Programmers Do it Wrong}
\begin{greenbox}{``race conditions only occur in books''}
  \texttt{RaceCondition.java} (when racers collide)
\end{greenbox}
\vspace*{2ex}
\begin{greenbox}{``parallelism increases speed''}
  \texttt{Two2One.java} (when one is better than two)
\end{greenbox}
\vspace*{2ex}
\begin{greenbox}{``deadlocks do not exist in practice''}
  \texttt{Philosophers.java} (when philosophers hang)
\end{greenbox}
\end{frame}

\begin{frame}
\frametitle{The Visibility Problem}
\begin{center}
\includegraphics[width=8.5cm]{pictures/visibility.png}
\end{center}
\begin{redbox}{}
The meaning of \texttt{volatile}
\end{redbox}
\end{frame}

\begin{frame}
\frametitle{The Java Memory Model}
\begin{center}
\includegraphics[width=8.5cm]{pictures/happens_before.png}
\end{center}
\begin{redbox}{}
The \emph{happens-before} relation holds in many other situations as well
\end{redbox}
\end{frame}

\begin{frame}
\frametitle{The Thread-Safeness Problem}

\begin{itemize}
\item thread-safe libraries are being developed nowadays
      \begin{itemize}
      \item their classes should be thread-safe
      \end{itemize}
\item but what does thread-safeness mean exactly?
      \begin{itemize}
      \item it can be used in a multithreaded way?
      \item any multithreaded execution can be rephrased as a sequential execution?
      \end{itemize}
\item verifying thread-safeness is still impossible today
      \begin{itemize}
      \item immutable classes are thread-safe!
      \item use thread-safe classes from the Java standard library
      \item for more complex cases, there are heuristics
      \end{itemize}
\item annotations \texttt{@ThreadSafe} and \texttt{@Immutable} can be helpful
\end{itemize}

\end{frame}

\begin{frame}
\frametitle{The GUI Problem}

\begin{itemize}
\item GUI toolkits are normally monothreaded (Swing, Android, \ldots)
\item operations on widgets must only be performed from the graphical thread
\item long-running operations block the graphical thread
\item they must be offloaded to worker threads
\item when a worker thread terminates
  \begin{itemize}
  \item it must be able to notify the user through the graphical thread
  \item views might have disappeared in the meanwhile (Android)
  \end{itemize}
\item methods can be annoted for clarity as \texttt{@UiThread}
  or \texttt{@WorkerThread}
\end{itemize}

\end{frame}

\begin{frame}
\frametitle{The \texttt{@GuardedBy}/\texttt{@Holding} Annotations}

\begin{greenbox}{\texttt{@GuardedBy}}
States that a field or parameter is only accessed by holding a lock
\begin{itemize}
\item introduced by Brian Goetz
\item used for the NASA PathFinder project
\item partially checked and inferred by some analysis tools
\end{itemize}
\end{greenbox}

\vspace*{2ex}

\begin{greenbox}{\texttt{@Holding}}
States that a method is only called by holding a lock
\begin{itemize}
  \item partially checked and inferred by some analysis tools
\end{itemize}
\end{greenbox}

\end{frame}

\begin{frame}\frametitle{Possibilities for \texttt{@GuardedBy}/\texttt{@Holding}}

\begin{greenbox}{\texttt{@GuardedBy/@Holding("this")}}
the lock on the receiver of a non-static field or method must be held
\end{greenbox}

\vspace*{2ex}

\begin{greenbox}{\texttt{@GuardedBy("itself")}}
the lock on the same parameter or field must be held
\end{greenbox}

\vspace*{2ex}

\begin{greenbox}{@GuardedBy/@Holding("field-name")}
the lock on the named field of the receiver of a non-static field or method must be held
\end{greenbox}

\vspace*{2ex}

\begin{greenbox}{@GuardedBy/@Holding("Class.field-name")}
the lock on the named static field of the named class must be held
\end{greenbox}

\end{frame}

\begin{frame}\frametitle{Possibilities for \texttt{@GuardedBy}/\texttt{@Holding}}

\begin{greenbox}{@GuardedBy/@Holding("Class.class")}
the lock of the unique class object representing the class named \texttt{Class} must be held
\end{greenbox}

\vspace*{2ex}

\begin{greenbox}{@GuardedBy/@Holding("foo()")}
the lock on the return value of the named instance method called on the receiver of a non-static field or method must be held. Method \texttt{foo} must return a reference type
\end{greenbox}

\vspace*{2ex}

\begin{greenbox}{@GuardedBy/@Holding("Class.foo()")}
the lock on the return value of the named static method of the named class must be held. Method \texttt{foo} must return a reference type
\end{greenbox}

\end{frame}

\begin{frame}
\frametitle{Tools for Concurrent Software Development}

\begin{itemize}
\item the standard \texttt{-Xprof} Java profiler is a basic tool for simple
      profiling: identifies blocking time and deadlocks
\item the YourKit Java profiler provides detailed information on block time
      and monitor usage and identifies deadlocks
\item static checkers such as Julia and FindBugs
\end{itemize}

\end{frame}

\begin{frame}
\frametitle{References}
\begin{greenbox}{}
\begin{center}
\includegraphics[width=3cm]{pictures/jcip.jpg}
\hspace*{1cm}
\includegraphics[width=3cm]{pictures/ac.jpg}
\hspace*{1cm}
\includegraphics[width=3cm]{pictures/android_threading.png}
\end{center}
\end{greenbox}

\end{frame}

\end{document}

