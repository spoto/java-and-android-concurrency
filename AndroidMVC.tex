\documentclass[11pt]{beamer}  %% versione proiettore
%%\documentclass[11pt,handout]{beamer} %% versione stampa
\usepackage{lucidiJb-2ed}

\mode<article>
{
  \usepackage{fullpage}
  \usepackage{hyperref}
}

\mode<presentation>
{
  \setbeamertemplate{background canvas}[vertical shading][bottom=red!10,top=blue!10]
  \usetheme{Android}
  \usefonttheme[onlysmall]{structurebold}
}

\title{Introduction to Android Programming}
\author{Fausto Spoto}
\subtitle{Java and Android Concurrency}
\institute{Universit\`a di Verona, Italy}
\date{April 2017}

\setbeamercovered{invisible}

\begin{document}

\frame{\titlepage}

\begin{frame}
\frametitle{Android Programming is Difficult}

\begin{greenbox}{}
Native Android programming is performed is Java.
Most people think it is consequently
simple UI design with little underlying logic
\end{greenbox}

\vspace*{1ex}
In reality, Android applications must
%
\begin{itemize}
\item support different devices and orientations
\item make heavy use of concurrency
\item deal with components that are created and destroyed by the framework
\end{itemize}

We will write a sample program that solves these problems in a generic way

\end{frame}

\begin{frame}\frametitle{References}
Some of this material has been taken from:
\begin{itemize}
\item \url{https://developer.android.com/training/index.html}
\item \textit{Head First Design Patterns}, 2004, O'Reilly Media
\item \textit{Android Programming: The Big Nerd Ranch Guide}, 2015, Financial Times/Prentice Hall
\end{itemize}

The Android Studio project of the Flickr Gallery sample application can be cloned from
\url{git@github.com:spoto/Flickr-Gallery.git}
\end{frame}

\begin{frame}\frametitle{User Interfaces: the Composite Pattern}
%
\begin{center}
\includegraphics[width=10cm]{pictures/viewgroup.png}
\end{center}
%
\end{frame}

\begin{frame}\frametitle{User Interfaces: Declarative Definition in XML}
%
File \texttt{res/layout/activity\_main.xml}
%
\begin{center}
\includegraphics[width=12cm]{pictures/xml_layout.png}
\end{center}
%
\end{frame}

\begin{frame}\frametitle{String Resources}
%
File \texttt{res/values/strings.xml}
%
\begin{center}
\includegraphics[width=12cm]{pictures/xml_strings.png}
\end{center}
%
\end{frame}

\begin{frame}\frametitle{This is the Resulting User Interface}
%
\begin{center}
\includegraphics[width=12cm]{pictures/edittext_gravity.png}
\end{center}
%
\end{frame}

\begin{frame}\frametitle{The Same Application on Different Devices}
%
\begin{center}
\includegraphics[width=12cm]{pictures/fragments-screen-mock.png}
\end{center}
%
\end{frame}

\begin{frame}\frametitle{Activities: UI Screens}
%
\begin{center}
\includegraphics[width=12cm]{pictures/activity_lifecycle.png}
\end{center}
%
\end{frame}

\begin{frame}\frametitle{Fragments: Self-Consistent Portions of UI Screens}
%
\begin{center}
\includegraphics[width=12cm]{pictures/fragment_lifecycle.jpg}
\end{center}
%
\end{frame}

\begin{frame}\frametitle{The Main Problems of Android Programming}

\begin{itemize}
\item The same code must deal with distinct UI
\item Activities and fragments can be suddenly destroyed and recreated
      \begin{itemize}
        \item their state must be saved!
      \end{itemize}
\item Activities and fragments events are run in the UI (aka main) thread
      \begin{itemize}
        \item they must be very short!
      \end{itemize}
\item Long running tasks must be delegated to background thread-like processors
\item If the user is not interacting with the application, its activities and
      fragments might be destroyed,
      also when the application is running a long background task
\item When such background tasks terminate, the starting activity or fragment might long be dead
\end{itemize}

\end{frame}

\begin{frame}\frametitle{Golden Rules of Android Programming}

\begin{pinkbox}{Activities and fragments are views}
They should be stateless (no fields!). If stateful, their state
should be related to their visualization only, not to the computation
\end{pinkbox}

\vspace*{1ex}

\begin{pinkbox}{Activities and fragments should never be stored in state}
Or otherwise they will not be garbage collected after the framework destroys them
(they get \alert{leaked})
\end{pinkbox}

\vspace*{1ex}

\begin{pinkbox}{Give preference to \texttt{Services} rathen than \texttt{ASyncTasks}}
Services increase the priority of the application and have a stable semantics
\end{pinkbox}

\vspace*{1ex}

\begin{pinkbox}{Use the Model/View/Controller design pattern}
Background tasks are part of the controller and communicate to
the model the results of their computation. The model notifies
all views existing at that moment
\end{pinkbox}

\end{frame}

\begin{frame}\frametitle{The Flickr Gallery Application}

\begin{center}
  {\Large The Flickr Gallery Application}
\end{center}

\vspace*{8ex}
\begin{pinkbox}{This application needs a free Flickr API key for identification}
Each key is limited to 3600 queries per hour. Get one for free from \url{https://www.flickr.com/services/api/misc.api_keys.html}
and copy it into \texttt{res/values/strings.xml}
\end{pinkbox}

\end{frame}

\begin{frame}\frametitle{Flickr Gallery on a Phone: Master or Detail}

\begin{center}
\includegraphics[width=4cm]{pictures/flicker_gallery_phone_1.png}
\hspace*{5ex}
\includegraphics[width=4cm]{pictures/flicker_gallery_phone_2.png}
\end{center}

\end{frame}

\begin{frame}\frametitle{Flickr Gallery on a Tablet: Master and Detail}

\begin{center}
\includegraphics[width=12cm]{pictures/flicker_gallery_tablet.png}
\end{center}

\end{frame}

\begin{frame}\frametitle{Spot the Difference}
%
\begin{center}
\begin{tabular}{c|c}
Phone & Tablet\\\hline\hline
title selection replaces with picture & title selection shows picture \\\hline
title shown below picture & title highlighted in list of titles \\\hline
menu name not shown & menu name shown\\\hline
\end{tabular}
\end{center}
%
\end{frame}

\begin{frame}\frametitle{Background Tasks}

Flickr Gallery spawns two kinds of background tasks:

\begin{description}
\item[load single picture]
\item[reload list of pictures]
\end{description}

\begin{itemize}
\item they run to completion also if activities and fragments are destroyed
      and recreated while the task runs
\item they run to completion also if the application is put to the background
\end{itemize}

\end{frame}

\begin{frame}\frametitle{The Model/View/Controller Design Pattern}

\begin{center}
\includegraphics[width=9.5cm]{pictures/mvc.jpg}
\end{center}

\end{frame}

\begin{frame}\frametitle{Thread Boundaries}

The suggested approach is to respect the following thread boundaries:

\begin{itemize}
\item The view runs in \texttt{@UiThread}
\item The controller runs in the \texttt{@UiThread} unless it needs to spawn
      background tasks, that run in a \texttt{@WorkerThread}
\item The model runs in the \texttt{@UiThread} or \texttt{@WorkerThread},
      hence access to its state must be \texttt{synchronized}
\end{itemize}

\end{frame}

\begin{frame}\frametitle{1) View Tells Controller: The User Did Something}
\begin{itemize}
\item \texttt{@UiThread void onTitleSelected(int position)}
\item \texttt{@UiThread void onPictureRequired(Context context, String url)}
\item \texttt{@UiThread void onTitlesReloadRequest(Context context)}
\end{itemize}
\begin{center}
\includegraphics[width=7cm]{pictures/mvc.jpg}
\end{center}
\end{frame}

\begin{frame}\frametitle{2) Controller Tells Model: Change Your State}
\begin{itemize}
\item \texttt{@WorkerThread void setPictures(Iterable<Picture> pictures)}
\item \texttt{@WorkerThread void setBitmap(String url, Bitmap bitmap)}
\end{itemize}
\begin{center}
\includegraphics[width=7cm]{pictures/mvc.jpg}
\end{center}
\end{frame}

\begin{frame}\frametitle{3) Controller Tells View: Change Your Display}
\begin{itemize}
\item \texttt{@UiThread void showPicture(int position)}
\end{itemize}
\begin{center}
\includegraphics[width=7cm]{pictures/mvc.jpg}
\end{center}
\end{frame}

\begin{frame}[fragile]\frametitle{4) Model Tells View: I Have Changed}
\begin{itemize}
\item \texttt{@UiThread void onModelChanged(Event event)}
{\small\begin{verbatim}
public enum Event {
  PICTURES_LIST_CHANGED,
  BITMAP_CHANGED
};
\end{verbatim}}

\end{itemize}
\begin{center}
\includegraphics[width=7cm]{pictures/mvc.jpg}
\end{center}
\end{frame}

\begin{frame}\frametitle{5) View Asks Model: I Need Your State Information}
\begin{itemize}
\item \texttt{@UiThread String[] getTitles()}
\item \texttt{@UiThread Bitmap getBitmap(int position)}
\item \texttt{@UiThread String getUrl(int position)}
\end{itemize}
\begin{center}
\includegraphics[width=7cm]{pictures/mvc.jpg}
\end{center}
\end{frame}

\begin{frame}\frametitle{The MVC Triple 1/3}
\begin{itemize}
\item The Model contains information about pictures and their titles
\begin{itemize}
\item \texttt{con.hotmoka.android.gallery.model.Pictures}
\end{itemize}
\item The View is an Android custom view interacting with the user
\begin{itemize}
\item \texttt{com.hotmoka.android.gallery.view.GalleryLayout}
\end{itemize}
\item The Controller is a wrapper to an Android service
\begin{itemize}
\item \texttt{com.hotmoka.android.gallery.controller.Controller}
\end{itemize}
\end{itemize}

\begin{greenbox}{Registration/Unregistration of Views}
Views register and unregister to the triple, since their existence is controlled
by the Android framework
\end{greenbox}

\end{frame}

\begin{frame}\frametitle{The MVC Triple 2/3}
\begin{center}
\includegraphics[width=12cm]{pictures/MVC_1.png}
\end{center}
\end{frame}

\begin{frame}\frametitle{The MVC Triple 3/3}
\begin{center}
\includegraphics[width=12cm]{pictures/MVC_2.png}
\end{center}
\end{frame}

\begin{frame}\frametitle{The Model of the Flickr Gallery App 1/4}
It must implement 2) and 5)
\begin{center}
\includegraphics[width=12cm]{pictures/Pictures1.png}
\end{center}
\end{frame}

\begin{frame}\frametitle{The Model of the Flickr Gallery App 2/4}
\begin{center}
\includegraphics[width=12cm]{pictures/Pictures2.png}
\end{center}

\begin{center}
Do not forget to synchronize also for read operations
\end{center}

\end{frame}

\begin{frame}\frametitle{The Model of the Flickr Gallery App 3/4}
\begin{center}
\includegraphics[width=9.5cm]{pictures/Pictures3.png}
\end{center}

\begin{center}
Minimize the length of mutual exclusion
\end{center}

\end{frame}

\begin{frame}\frametitle{The Model of the Flickr Gallery App 4/4}
\begin{center}
\includegraphics[width=11cm]{pictures/Pictures4.png}
\end{center}

\begin{center}
Make sure view methods are called from the UI thread
\end{center}

\end{frame}

\begin{frame}\frametitle{Titles Reload on a Phone}
\begin{center}
\includegraphics[width=6cm]{pictures/GalleryLayout_phone.png}
\end{center}
\end{frame}

\begin{frame}\frametitle{Titles Reload on a Phone}
\begin{center}
\includegraphics[width=6cm]{pictures/GalleryLayout_reload_phone_step1.png}
\end{center}
On user touch, the framework notifies the \texttt{TitlesFragment}
\end{frame}

\begin{frame}\frametitle{Titles Reload on a Phone}
\begin{center}
\includegraphics[width=11cm]{pictures/GalleryLayout_reload_phone_step2.png}\\
The \texttt{TitleFragment} notifies the controller of the event
\end{center}
\end{frame}

\begin{frame}\frametitle{Titles Reload on a Phone}
\begin{center}
\includegraphics[width=11cm]{pictures/GalleryLayout_reload_phone_step3.png}\\
The controller spawns a background task that will update the model
\end{center}
\end{frame}

\begin{frame}\frametitle{Titles Reload on a Phone}
\begin{center}
\includegraphics[width=11cm]{pictures/GalleryLayout_reload_phone_step4.png}\\
The model notifies the \texttt{GalleryLayout} that it has changed
\end{center}
\end{frame}

\begin{frame}\frametitle{Titles Reload on a Phone}
\begin{center}
\includegraphics[width=11cm]{pictures/GalleryLayout_reload_phone_step5.png}\\
The \texttt{GalleryLayout} delegates to its fragment
\end{center}
\end{frame}

\begin{frame}\frametitle{Titles Reload on a Phone}
\begin{center}
\includegraphics[width=11cm]{pictures/GalleryLayout_reload_phone_step6.png}\\
The \texttt{TitlesFragment} updates its list of titles
\end{center}
\end{frame}

\begin{frame}\frametitle{Titles Reload on a Tablet}
\begin{center}
\includegraphics[width=6cm]{pictures/GalleryLayout_tablet.png}
\end{center}
\end{frame}

\begin{frame}\frametitle{Titles Reload on a Tablet}
\begin{center}
\includegraphics[width=6cm]{pictures/GalleryLayout_reload_tablet_step1.png}
\end{center}
On user touch, the framework notifies the \texttt{TitlesFragment}
\end{frame}

\begin{frame}\frametitle{Titles Reload on a Tablet}
\begin{center}
\includegraphics[width=11cm]{pictures/GalleryLayout_reload_tablet_step2.png}\\
The \texttt{TitleFragment} notifies the controller of the event
\end{center}
\end{frame}

\begin{frame}\frametitle{Titles Reload on a Tablet}
\begin{center}
\includegraphics[width=11cm]{pictures/GalleryLayout_reload_tablet_step3.png}\\
The controller spawns a background task that will update the model
\end{center}
\end{frame}

\begin{frame}\frametitle{Titles Reload on a Tablet}
\begin{center}
\includegraphics[width=11cm]{pictures/GalleryLayout_reload_tablet_step4.png}\\
The model notifies the \texttt{GalleryLayout} that it has changed
\end{center}
\end{frame}

\begin{frame}\frametitle{Titles Reload on a Tablet}
\begin{center}
\includegraphics[width=11cm]{pictures/GalleryLayout_reload_tablet_step5.png}\\
The \texttt{GalleryLayout} delegates to its fragments
\end{center}
\end{frame}

\begin{frame}\frametitle{Titles Reload on a Tablet}
\begin{center}
\includegraphics[width=11cm]{pictures/GalleryLayout_reload_tablet_step6.png}\\
The \texttt{TitlesFragment} updates its list, the \texttt{PictureFragment} clears
\end{center}
\end{frame}

\begin{frame}\frametitle{Title Selection on a Phone}
\begin{center}
\includegraphics[width=6cm]{pictures/GalleryLayout_phone_step1.png}\\
On user touch, the framework notifies the \texttt{TitlesFragment}
\end{center}
\end{frame}

\begin{frame}\frametitle{Title Selection on a Phone}
\begin{center}
\includegraphics[width=11cm]{pictures/GalleryLayout_phone_step2.png}\\
The \texttt{TitleFragment} notifies the controller of the event
\end{center}
\end{frame}

\begin{frame}\frametitle{Title Selection on a Phone}
\begin{center}
\includegraphics[width=11cm]{pictures/GalleryLayout_phone_step3.png}\\
The controller asks the \texttt{GalleryLayout} to show a picture
\end{center}
\end{frame}

\begin{frame}\frametitle{Title Selection on a Phone}
\begin{center}
\includegraphics[width=11cm]{pictures/GalleryLayout_phone_step4.png}\\
The \texttt{GalleryLayout} replaces the fragment with a \texttt{PictureFrament}
\end{center}
\end{frame}

\begin{frame}\frametitle{Title Selection on a Phone}
\begin{center}
\includegraphics[width=11cm]{pictures/GalleryLayout_phone_step5.png}\\
The framework notifies the fragment to start interacting
\end{center}
\end{frame}

\begin{frame}\frametitle{Title Selection on a Phone}
\begin{center}
\includegraphics[width=11cm]{pictures/GalleryLayout_phone_step6.png}\\
The \texttt{PictureFragment} notifies the controller that it needs a picture
\end{center}
\end{frame}

\begin{frame}\frametitle{Title Selection on a Phone}
\begin{center}
\includegraphics[width=11cm]{pictures/GalleryLayout_phone_step7.png}\\
The controller spawns a background task that will update the model
\end{center}
\end{frame}

\begin{frame}\frametitle{Title Selection on a Phone}
\begin{center}
\includegraphics[width=11cm]{pictures/GalleryLayout_phone_step8.png}\\
The model notifies the \texttt{GalleryLayout} that it has changed
\end{center}
\end{frame}

\begin{frame}\frametitle{Title Selection on a Phone}
\begin{center}
\includegraphics[width=11cm]{pictures/GalleryLayout_phone_step9.png}\\
The \texttt{GalleryLayout} delegates to its fragment
\end{center}
\end{frame}

\begin{frame}\frametitle{Title Selection on a Phone}
\begin{center}
\includegraphics[width=11cm]{pictures/GalleryLayout_phone_step10.png}\\
The \texttt{PictureFragment} shows the picture
\end{center}
\end{frame}

\begin{frame}\frametitle{Title Selection on a Tablet}
\begin{center}
\includegraphics[width=6cm]{pictures/GalleryLayout_tablet_step1.png}\\
On user touch, the framework notifies the \texttt{TitlesFragment}
\end{center}
\end{frame}

\begin{frame}\frametitle{Title Selection on a Tablet}
\begin{center}
\includegraphics[width=11cm]{pictures/GalleryLayout_tablet_step2.png}\\
The \texttt{TitleFragment} notifies the controller of the event
\end{center}
\end{frame}

\begin{frame}\frametitle{Title Selection on a Tablet}
\begin{center}
\includegraphics[width=11cm]{pictures/GalleryLayout_tablet_step3.png}\\
The controller asks the \texttt{GalleryLayout} to show a picture
\end{center}
\end{frame}

\begin{frame}\frametitle{Title Selection on a Tablet}
\begin{center}
\includegraphics[width=11cm]{pictures/GalleryLayout_tablet_step4.png}\\
The \texttt{GalleryLayout} delegates to the \texttt{PictureFrament}
\end{center}
\end{frame}

\begin{frame}\frametitle{Title Selection on a Tablet}
\begin{center}
\includegraphics[width=11cm]{pictures/GalleryLayout_tablet_step5.png}\\
The \texttt{PictureFragment} notifies the controller that it needs a picture
\end{center}
\end{frame}

\begin{frame}\frametitle{Title Selection on a Tablet}
\begin{center}
\includegraphics[width=11cm]{pictures/GalleryLayout_tablet_step6.png}\\
The controller spawns a background task that will update the model
\end{center}
\end{frame}

\begin{frame}\frametitle{Title Selection on a Tablet}
\begin{center}
\includegraphics[width=11cm]{pictures/GalleryLayout_tablet_step7.png}\\
The model notifies the \texttt{GalleryLayout} that it has changed
\end{center}
\end{frame}

\begin{frame}\frametitle{Title Selection on a Tablet}
\begin{center}
\includegraphics[width=11cm]{pictures/GalleryLayout_tablet_step8.png}\\
The \texttt{GalleryLayout} delegates to its fragments
\end{center}
\end{frame}

\begin{frame}\frametitle{Title Selection on a Tablet}
\begin{center}
\includegraphics[width=11cm]{pictures/GalleryLayout_tablet_step9.png}\\
The \texttt{TitlesFragment} ignores it, the \texttt{PictureFragment} shows the picture
\end{center}
\end{frame}

\begin{frame}\frametitle{The View of the Flickr Gallery App}
It must implement 3) and 4)
\begin{center}
\includegraphics[width=9cm]{pictures/GalleryLayout.png}
\end{center}
\end{frame}

\begin{frame}\frametitle{Dealing with Polymorphic UI's}

\begin{pinkbox}{Java is an OO language}
Let us define two implementations of the view
\end{pinkbox}

\begin{center}
\begin{tabular}{c|c}
phone & tablet \\\hline\hline
{\scriptsize\texttt{res/layout/activity\_gallery.xml}} & {\scriptsize\texttt{res/layout\_large/activity\_gallery.xml}} \\\hline
\texttt{*.single.GalleryLayout} & \texttt{*.two.GalleryLayout}
\end{tabular}
\end{center}

\begin{itemize}
\item The Android framework will automatically select the layout for
      the suitable configuration
\item This layout will instantiate distinct implementations of the view
\end{itemize}

\begin{center}
No tests, purely OO palm oil free code!
\end{center}

\end{frame}

\begin{frame}\frametitle{The Phone Layout of Flickr Gallery}
\begin{center}
\includegraphics[width=12cm]{pictures/activity_gallery_xml_phone.png}
\end{center}
\end{frame}

\begin{frame}\frametitle{The Tablet Layout of Flickr Gallery}
\begin{center}
\includegraphics[width=10cm]{pictures/activity_gallery_xml_tablet.png}
\end{center}
\end{frame}

\begin{frame}\frametitle{The Only Activity of the Flickr Gallery App}
\begin{center}
\includegraphics[width=12cm]{pictures/GalleryActivity.png}
\end{center}
\end{frame}

\begin{frame}\frametitle{The Creation of the App UI}

\begin{center}
  The Android framework creates \texttt{GalleryActivity}\\
  $\downarrow$\\
  \texttt{GalleryActivity} inflates the right \texttt{activity\_gallery.xml}\\
  $\downarrow$\\
  each \texttt{activity\_gallery.xml} specifies a distinct implementation
  of \texttt{GalleryLayout} (a custom Android view) and distinct versions of the
  fragments that they contain
\end{center}

\end{frame}

\begin{frame}\frametitle{The View 1/4}

\begin{greenbox}{Phone: a \texttt{FrameLayout} that contains a swappable fragment}
\includegraphics[width=11.5cm]{pictures/GalleryLayout_phone_1.png}
\end{greenbox}

\begin{greenbox}{Tablet: a \texttt{LinearLayout} that contains two fragments}
\includegraphics[width=10cm]{pictures/GalleryLayout_tablet_1.png}
\end{greenbox}
\end{frame}

\begin{frame}\frametitle{The View 2/4}

\begin{greenbox}{Phone: registration/unregistration}
\includegraphics[width=12cm]{pictures/GalleryLayout_phone_2.png}
\end{greenbox}

\begin{greenbox}{Tablet: registration/unregistration}
Identical, but without fragment transaction
\end{greenbox}

\end{frame}

\begin{frame}\frametitle{The View 3/4}

\begin{greenbox}{Phone: showing a picture replaces the fragment}
\includegraphics[width=12cm]{pictures/GalleryLayout_phone_3.png}
\end{greenbox}

\begin{greenbox}{Tablet: showing a picture delegates to the \texttt{PictureFragment}}
\includegraphics[width=10cm]{pictures/GalleryLayout_tablet_3.png}
\end{greenbox}

\end{frame}

\begin{frame}\frametitle{The View 4/4}

\begin{greenbox}{Phone: delegates model updates to the only fragment}
\includegraphics[width=12cm]{pictures/GalleryLayout_phone_4.png}
\end{greenbox}

\begin{greenbox}{Tablet: delegates model updates to both fragments}
\includegraphics[width=12cm]{pictures/GalleryLayout_tablet_4.png}
\end{greenbox}

\end{frame}

\begin{frame}\frametitle{The View: \texttt{TitlesFragment} and \texttt{PictureFragment}}

\begin{itemize}
\item The phone layout swaps between the two
\item The tablet layout shows both at the same time
\item The fragments behave slightly differently in either layout
\begin{itemize}
\item we provide shared abstract superclasses, subclassed for the two layouts
\end{itemize}
\begin{center}
\includegraphics[width=11cm]{pictures/fragments.png}
\end{center}
\end{itemize}

\end{frame}

\begin{frame}\frametitle{Shared \texttt{TitlesFragment}: Show the Titles}
\begin{center}
\includegraphics[width=12cm]{pictures/TitlesFragment_1.png}
\end{center}
\end{frame}

\begin{frame}\frametitle{Shared \texttt{TitlesFragment}: The Options Menu}
\begin{center}
\includegraphics[width=10.5cm]{pictures/TitlesFragment_2.png}
\end{center}
\end{frame}

\begin{frame}\frametitle{Shared \texttt{TitlesFragment}: List Tap and List Update}
\begin{center}
\includegraphics[width=12cm]{pictures/TitlesFragment_3.png}
\end{center}
\end{frame}

\begin{frame}\frametitle{Shared \texttt{PictureFragment}: A View with State}
\begin{center}
\includegraphics[width=12cm]{pictures/PictureFragment_1.png}
\end{center}
\end{frame}

\begin{frame}\frametitle{Shared \texttt{PictureFragment}: Require a Picture}
\begin{center}
\includegraphics[width=10.5cm]{pictures/PictureFragment_2.png}
\end{center}
\end{frame}

\begin{frame}\frametitle{Shared \texttt{PictureFragment}: Show or Download a Picture}
\begin{center}
\includegraphics[width=12cm]{pictures/PictureFragment_3.png}
\end{center}
\end{frame}

\begin{frame}\frametitle{Shared \texttt{PictureFragment}: React to Model Changes}
\begin{center}
\includegraphics[width=12cm]{pictures/PictureFragment_4.png}
\end{center}
\end{frame}

\begin{frame}\frametitle{Fragments Specialization for the Phone}
\begin{center}
\includegraphics[width=12cm]{pictures/TitlesFragment_one.png}
\end{center}
\begin{center}
\includegraphics[width=12cm]{pictures/PictureFragment_one.png}
\end{center}
\end{frame}

\begin{frame}\frametitle{Fragments Specialization for Tablet}
\begin{center}
\includegraphics[width=12cm]{pictures/TitlesFragment_two.png}
\end{center}
\begin{center}
\includegraphics[width=12cm]{pictures/PictureFragment_two.png}
\end{center}
\end{frame}

\begin{frame}\frametitle{The Controller of the Flickr Gallery App}
%
It implements 1). Keeps track of the number of running background tasks
\begin{center}
\includegraphics[width=9.7cm]{pictures/controller_flicker.png}
\end{center}
\end{frame}

\begin{frame}\frametitle{The \texttt{IntentService} of the Flickr Gallery App 1/2}
It receives intents to process on a worker thread, \alert{sequentially}
\begin{center}
\includegraphics[width=10.5cm]{pictures/ControllerService_1.png}
\end{center}
\end{frame}

\begin{frame}\frametitle{The \texttt{IntentService} of the Flickr Gallery App 2/2}
\begin{center}
\includegraphics[width=12cm]{pictures/ControllerService_2.png}
\end{center}
\end{frame}

\begin{frame}\frametitle{Downloading the List of the Latest Pictures 1/3}
\begin{center}
\includegraphics[width=12cm]{pictures/ListOfPicturesFetcher_1.png}
\end{center}
\end{frame}

\begin{frame}\frametitle{Downloading the List of the Latest Pictures 2/3}
\begin{itemize}
\item Query {\scriptsize https://api.flickr.com/services/rest/?method=flickr.photos.getRecent}
      {\scriptsize \&api\_key=...\&extras=url\_z\&per\_page=40}
\item Parse XML into \texttt{Picture}s
\end{itemize}
\begin{center}
\includegraphics[width=9cm]{pictures/ListOfPicturesFetcher_2.png}
\end{center}
\end{frame}

\begin{frame}\frametitle{Downloading the List of the Latest Pictures 3/3}
\begin{center}
\includegraphics[width=12cm]{pictures/ListOfPicturesFetcher_3.png}
\end{center}
\end{frame}

\end{document}
