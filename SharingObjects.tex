\documentclass[11pt]{beamer}  %% versione proiettore
%%\documentclass[11pt,handout]{beamer} %% versione stampa
\usepackage{lucidiJb-2ed}

\mode<article>
{
  \usepackage{fullpage}
  \usepackage{hyperref}
}

\mode<presentation>
{
  \setbeamertemplate{background canvas}[vertical shading][bottom=red!10,top=blue!10]
  \usetheme{SharingObjects}
  \usefonttheme[onlysmall]{structurebold}
}

\title{Sharing Objects}
\author{Fausto Spoto}
\subtitle{Java and Android Concurrency}
\institute{Universit\`a di Verona, Italy}
\date{February 2017}

\setbeamercovered{invisible}

\begin{document}

\frame{\titlepage}

\begin{frame}[fragile]
  \frametitle{Visibility}

  Synchronization has two goals:
  \begin{itemize}
  \item ensure mutual exclusion (everybody knows this)
  \item ensure \alert{visibility} (nobody knows this)
  \end{itemize}

  What does this print?

  {\scriptsize
\begin{verbatim}
public class NoVisibility {
    private static boolean ready;
    private static int number;

    private static class ReaderThread extends Thread {
        public void run() {
            while (!ready) Thread.yield();
            System.out.println(number);
        }
    }

    public static void main(String[] args) {
        new ReaderThread().start();
        number = 42;
        ready = true;
    }
}
\end{verbatim}}
\end{frame}

\begin{frame}
  \frametitle{The Heisenberg Principle of Concurrent Programming}

  \begin{greenbox}{}
    In the absence of synchronization, the compiler, processor, and runtime can do some downright
    weird things to the order in which operations appear to execute. Attempts to reason
    about the order in which memory actions "must" happen in insufficiently
    synchronized multithreaded programs will almost certainly be incorrect
  \end{greenbox}

  \bigskip
  A field might contain any of the values ever written into the field, but not necessarily
  the last one (\alert{out-of-thin-air safety}). For \texttt{long} and \texttt{double}
  fields, even a value never written into the field might be seen!
\end{frame}

\begin{frame}[fragile]
  \frametitle{Synchronization Guarantees Visibility}

\begin{verbatim}
@NotThreadSafe
public class MutableInteger {
    private int value;

    public int get() {
        return value;
    }

    public void set(int value) {
        this.value = value;
    }
}
\end{verbatim}

\end{frame}

\begin{frame}[fragile]
  \frametitle{Synchronization Guarantees Visibility}

\begin{alltt}
@NotThreadSafe
public class MutableInteger \{
    private int value;

    public int get() \{
        return value;
    \}

    public \alert{synchronized} void set(int value) \{
        this.value = value;
    \}
\}
\end{alltt}

\end{frame}

\begin{frame}[fragile]
  \frametitle{Synchronization Guarantees Visibility}

\begin{alltt}
\alert{@ThreadSafe}
public class SynchronizedInteger \{
    private \alert{@GuardedBy("this")} int value;

    public \alert{synchronized} int get() \{
        return value;
    \}

    public synchronized void set(int value) \{
        this.value = value;
    \}
\}
\end{alltt}

\end{frame}

\begin{frame}[fragile]
  \frametitle{Locking, Visibility and \texttt{volatile} Fields}

  \begin{greenbox}{Locking guarantees visibility}
    When thread $A$ executes a \texttt{synchronized} block,
    and subsequently thread $B$ enters a \texttt{synchronized}
    block guarded by the same lock, the values of variables that
    were visible to $A$ prior to realeasing the lock
    are guaranteed to be visible to $B$ upon acquiring the lock
  \end{greenbox}

  \bigskip
  \begin{greenbox}{\texttt{volatile} guarantees visibility}
    When thread $A$ writes to a volatile field and subsequently thread $B$
    reads the same field, the values of \alert{all} variables that were visible
    to $A$ prior to writing become visible to $B$ after reading
  \end{greenbox}

  {\small
\begin{verbatim}
  volatile boolean asleep;

  void tryToSleep() {
    while (!asleep)
      countSomeSheep();
  }
\end{verbatim}
}

\end{frame}

\begin{frame}
  \frametitle{Use \texttt{volatile} Variables Sparingly}

  \begin{itemize}
  \item they have higher access cost
  \item compound operations are not atomic: \texttt{count++}
  \end{itemize}

  \medskip
  \begin{greenbox}{}
    Locking can guarantee both visibility and atomicity; \texttt{volatile} variables
    can only guarantee visibility
  \end{greenbox}

  \medskip
  Use \texttt{volatile} variables only if
  \begin{enumerate}
  \item writes do not depend on previous value or only one thread performs writes
  \item the variable does not participate in invariants with other variables
  \item locking is not needed for any other reason
  \end{enumerate}
  
\end{frame}

\begin{frame}
  \frametitle{Publication and Escape}

  \begin{greenbox}{Publication}
    Publishing an object means making it available to code outside of its current scope:
    \begin{itemize}
    \item publishing internal state can compromise encapsulation and thread-safety
    \item publishing not fully constructed objects can compromise thread-safety
    \end{itemize}
  \end{greenbox}

  \bigskip
  \begin{greenbox}{Escape}
    An object that is published when it should not have been is said to have \alert{escaped}
    its intended scope
  \end{greenbox}
  
\end{frame}

\begin{frame}[fragile]
  \frametitle{Examples of Escape}

  \begin{greenbox}{Escape through public fields}
\begin{verbatim}
public static Set<Secret> knownSecrets;

public void initialize() {
  knownSecrets = new HashSet<Secret>();
}
\end{verbatim}
  \end{greenbox}

  \bigskip
  \begin{greenbox}{Escape through return value}
\begin{verbatim}
private String[] states = new String[]{ "AK", "AL" ... };

public String[] getStates() {
  return states;
}
\end{verbatim}
  \end{greenbox}

\end{frame}

\begin{frame}[fragile]
  \frametitle{Examples of Escape}

  \begin{greenbox}{Escape through overriddable methods}
\begin{verbatim}
private String[] states = new String[]{ "AK", "AL" ... };

...doSomething(states);

protected void doSomething(String[] ss) { ... }
\end{verbatim}
  \end{greenbox}

  \bigskip
  \begin{greenbox}{Escape through non-static inner classes}
\begin{verbatim}
public class ThisEscape {
  public ThisEscape(EventSource source) {
    source.registerListener(new EventListener() {
      public void onEvent(Event e) { doSomething(e); }
    });
  }
  ...
}
\end{verbatim}
  \end{greenbox}

\end{frame}

\begin{frame}
  \frametitle{Escape of Raw Objects}

  The examples with overriddable methods and non-static inner classes
  are particularly bad since they can allow a partially initialized object
  (\alert{raw}) to escape its constructor

  \medskip
  Partially initialized objects might not be usable from other threads,
  even if the class seems thread-safe

  \bigskip
  \begin{greenbox}{}
    Do not allow the \texttt{this} reference to escape during construction
  \end{greenbox}
  
\end{frame}

\begin{frame}
  \frametitle{Thread Confinement}

  \begin{greenbox}{}
    If an object is \alert{confined} to a thread, that is,
    it can only be accessed by that single thread, then there is no need to make it
    thread-safe, since all its uses are automatically thread-safe
  \end{greenbox}

  \bigskip
  \begin{greenbox}{Ad-hoc thread confinement}
  \begin{itemize}
  \item Java's Swing graphical library
  \item Android graphical library
  \item database connections from a connection pool
  \end{itemize}
  \end{greenbox}

  \bigskip
  \begin{greenbox}{Stack confinement}
    Local variables are intrinsically confined to the executing thread
    \begin{itemize}
    \item always for primitive values
    \item if not published, for reference values
    \end{itemize}
  \end{greenbox}
  
\end{frame}

\begin{frame}[fragile]
  \frametitle{Thread Confinement through \texttt{ThreadLocal}}

  \texttt{ThreadLocal}s are a sort of per-thread static fields.
  Calls to \alert{\texttt{get}} yield the value associated to the
  currently executing thread, either initialized through
  \alert{\texttt{initialValue}} or subsequently
  modified through \alert{\texttt{set}}
  
\begin{alltt}
private ThreadLocal<Connection> connectionHolder
  = new ThreadLocal<>() \{
    public Connection \alert{initialValue}() \{
      return DriverManager.getConnection(DB\_URL);
    \}
  \};

public Connection getConnection() \{
  return connectionHolder.\alert{get}();
\}
\end{alltt}
\end{frame}

\begin{frame}
  \frametitle{Immutability}

  \begin{greenbox}{Immutable objects are good}
    Immutable objects are thread-safe and can be shared and published without synchronization
  \end{greenbox}

  \bigskip
  \begin{greenbox}{An object is \alert{immutable} if}
    \begin{enumerate}
    \item its state cannot be modified after construction
    \item all its fields are \texttt{final}
    \item it is \alert{properly constructed} (\texttt{this} does not escape
      during construction)
    \end{enumerate}
  \end{greenbox}

\end{frame}

\begin{frame}[fragile]
  \frametitle{An Immutable Object Can Well Use Modifiable Objects}

  {\small
\begin{alltt}
\alert{@Immutable}
public final class ThreeStooges \{
  private final Set<String> stooges = new HashSet<>();

  public ThreeStooges() \{
    stooges.add("Moe");
    stooges.add("Larry");
    stooges.add("Curly");
  \}

  public boolean isStooge(String name) \{
    return stooges.contains(name);
  \}

  public String getStoogeNames() \{
    return stooges.toString();
  \}
\}
\end{alltt}
}

\end{frame}

\begin{frame}[fragile]
  \frametitle{Immutable Objects and Collections in State}

  Frequently, one is tempted to return a modifiable collection
  from the state of an object: this breaks encapsulation, makes the state escape and
  makes the object mutable:

\begin{alltt}
\alert{@Mutable}
public class C \{
  private final Set<Element> set = new HashSet<>();
  ...
  public Set<Element> getElements() \{
    \alert{return set};
  \}
\}
\end{alltt}

\end{frame}

\begin{frame}[fragile]
  \frametitle{Immutable Objects and Collections in State}

  \alert{Bad solution}: return a copy of the set
  \begin{itemize}
  \item the user might modify the copy and think this actually does something
  \end{itemize}

  \begin{alltt}
\alert{@Immutable}
public class C \{
  private final Set<Element> set = new HashSet<>();
  ...
  public Set<Element> getElements() \{
    \alert{return new HashSet<>(set)};
  \}
\}
\end{alltt}

\end{frame}

\begin{frame}[fragile]
  \frametitle{Immutable Objects and Collections in State}

  \alert{Bad solution}: return a \texttt{Collections.unmodifiableSet}
  \begin{itemize}
  \item the user might modify the copy, because he thinks this actually does something,
    and get an exception, but only at runtime
  \end{itemize}

  \begin{alltt}
\alert{@Immutable}
public class C \{
  private final Set<Element> set = new HashSet<>();
  ...
  public Set<Element> getElements() \{
    \alert{return Collections.unmodifiableSet(set)};
  \}
\}
\end{alltt}

\end{frame}

\begin{frame}[fragile]
  \frametitle{Immutable Objects and Collections in State}

  \alert{Good solution}: make the class iterable or return a new \texttt{Iterable} instead of a \texttt{Set}
  \begin{itemize}
  \item in 99\% of the cases, iterations is all the user wants to do
  \end{itemize}

  \begin{alltt}
\alert{@Immutable}
public class C \alert{implements Iterable<Element>} \{
  private final Set<Element> set = new HashSet<>();
  ...
  public Iterator<Element> \alert{iterator}() \{
    \alert{return set.iterator()};
  \}
\}
\end{alltt}

\end{frame}

\begin{frame}[fragile]
  \frametitle{Immutable Objects and Collections in State}

  \alert{Good solution}: use internal iteration, passing a task as an interface or
  lambda-expression

  \begin{alltt}
\alert{@Immutable}
public class C \{
  private final Set<Element> set = new HashSet<>();

  public interface Task \{
    void process(Element e);
  \}

  ...
  public void \alert{forEach}(Task task) \{
    \alert{for (Element e: set)
      task.process(e)};
  \}
\}
\end{alltt}

\end{frame}

\begin{frame}[fragile]
  \frametitle{Immutable Objects and Collections in State}

  \alert{Good solution}: define and return a new \texttt{ImmutableSet} copy
  \begin{itemize}
  \item for the 1\% of the cases, when the user wants to do more than iterate
  \end{itemize}

{\small\begin{alltt}
public interface ImmutableSet<E> extends Iterable<E> \{
  int size();
\}

\alert{@Immutable}
public class C \{
  private final Set<Element> set = new HashSet<>();
  ...
  public \alert{ImmutableSet<Element>} getElements() \{
    return \alert{new ImmutableSet<Element>() \{
      public int size() \{ return set.size(); \}
      public Iterator<Element> iterator() \{ return set.iterator(); \}
    \}};
  \}
\}
\end{alltt}}

\end{frame}

\begin{frame}
  \frametitle{Don't Be Afraid of Immutability}

  An immutable object has only a single state, but references to an immutable object can be
  updated!

  \medskip
  Moreover:
  \begin{itemize}
  \item no locking cost
  \item no defensive copies
  \item instances can be shared instead of duplicated
  \item reduced generational garbage-collection
  \end{itemize}
  
\end{frame}

\begin{frame}[fragile]
  \frametitle{An Immutable Object for the Factorization Cache}

{\small\begin{alltt}
\alert{@Immutable}
public class OneValueCache \{
  private final BigInteger lastNumber;
  private final BigInteger[] lastFactors;

  public OneValueCache(BigInteger i, BigInteger[] factors) \{
    lastNumber = i;
    lastFactors = Arrays.copyOf(factors, factors.length);
  \}

  public BigInteger[] getFactors(BigInteger i) \{
    if (lastNumber == null || !lastNumber.equals(i))
      return null;
    else
      return Arrays.copyOf(lastFactors, lastFactors.length);
  \}
\}
\end{alltt}}

\end{frame}

\begin{frame}[fragile]
  \frametitle{Cached Factorizer without Synchronization}

{\small\begin{alltt}
\alert{@ThreadSafe}
public class VolatileCachedFactorizer extends StatelessFactorizer \{
  private \alert{volatile} OneValueCache cache
      = new OneValueCache(null, null);

  @Override
  protected void doPost(HttpServletRequest request,
                        HttpServletResponse response) \{

    BigInteger i = extractFromRequest(request);
    BigInteger[] factors = cache.getFactors(i);
    if (factors == null) \{
      factors = factor(i);
      cache = new OneValueCache(i, factors);
    \}
    encodeIntoResponse(response, factors);
  \}
\}
\end{alltt}}

\end{frame}

\begin{frame}[fragile]
  \frametitle{Unsafe Publication}

  Very often, we do want to share objects. But \alert{publication must be done
  in a safe way}, or otherwise very weird things might happen

{\small\begin{verbatim}
public class StuffIntoPublic {
  public Holder holder;
  public void initialize() { holder = new Holder(42); }
}

public class Holder {
  private int n;
  public Holder(int n) { this.n = n; }
  public void assertSanity() {
    if (n != n) throw new AssertionError("This is false");
  }
}
\end{verbatim}}
  
\begin{greenbox}{}
\begin{enumerate}
\item \texttt{holder} might be seen to contain \texttt{null} or a stale, old \texttt{Holder}
\item \texttt{n}'s value might suddenly change, making the assertion fail
\end{enumerate}
\end{greenbox}

\end{frame}

\begin{frame}[fragile]
  \frametitle{Safe Publication}

{\small\begin{alltt}
public class StuffIntoPublic \{
  public \alert{volatile} Holder holder;
  public void initialize() \{ holder = new Holder(42); \}
\}

public class Holder \{
  private \alert{final} int n;
  public Holder(int n) \{ this.n = n; \}
  public void assertSanity() \{
    if (n != n) throw new AssertionError("This is false");
  \}
\}
\end{alltt}}

  Making just \texttt{n} \texttt{final} would only satisfy the assertion:
  the object is published,
  but the reference to it might be stale

  \bigskip
  Making \texttt{holder} \texttt{volatile} solves all problems
\end{frame}

\begin{frame}
  \frametitle{Safe Publication Idioms}

  Immutable objects can be published in any way. Mutable objects must be
  safely published, so that their internal state is guaranteed to be visible.
  This can happen in many ways:

  \begin{itemize}
  \item by storing the object into a \texttt{volatile} field
  \item by storing the object into a \texttt{final} field of a properly constructed object
  \item by initializing the object inside a class static initializer
  \item by storing the object into a properly guarded field
  \item by passing the object to a synchronized collection class from
    the standard Java library
  \end{itemize}
  
\end{frame}

\begin{frame}
  \frametitle{Exercise 1: Parallel Matrix Multiplication}

  Make the matrix multiplication constructor in the following code
  parallel, through the use of multithreading

  \begin{itemize}
  \item how much faster do you think it will run then on a $n$-core machine?
  \item once the multiplication is divided across different threads,
    why is the resulting matrix safely published to the calling thread?
  \end{itemize}
  
\end{frame}

\begin{frame}[fragile]
  \frametitle{Exercise 1: Sequential Matrix Multiplication}

{\scriptsize\begin{verbatim}
public class Matrix {
  private final double[][] elements;
  private final static Random random = new Random();

  public Matrix(int m, int n) {
    this.elements = new double[m][n];
    for (int x = 0; x < n; x++)
      for (int y = 0; y < m; y++)
        elements[y][x] = random.nextDouble() * 100.0 - 50.0;
  }

  public int getM() {
    return elements.length;
  }

  public int getN() {
    return elements[0].length;
  }
\end{verbatim}}

\end{frame}

\begin{frame}[fragile]
  \frametitle{Exercise 1: Sequential Matrix Multiplication}

{\small\begin{alltt}
  // \alert{modify this constructor, make it run in parallel!}
  private Matrix(Matrix left, Matrix right) \{
    int m = left.getM();
    int p = left.getN();
    int n = right.getN();

    this.elements = new double[m][n];
    for (int x = 0; x < n; x++)
      for (int y = 0; y < m; y++) \{
        double sum = 0.0;
        for (int k = 0; k < p; k++)
          sum += left.elements[y][k] * right.elements[k][x];
        this.elements[y][x] = sum;
      \}
  \}

  ...
\}
\end{alltt}}

\end{frame}

\begin{frame}
  \frametitle{Esercise 2: Implement a Swing GUI for the Chat Servlets}

  Build a Swing GUI for the chat servlets
  \begin{itemize}
  \item it should include a button that allows
    one to send a new chat message (author and text)
  \item and a text area that
    can be refreshed to show the last 20 messages from the chat
  \end{itemize}
\end{frame}

\end{document}
