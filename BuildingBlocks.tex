\documentclass[11pt]{beamer}  %% versione proiettore
%%\documentclass[11pt,handout]{beamer} %% versione stampa
\usepackage{lucidiJb-2ed}

\mode<article>
{
  \usepackage{fullpage}
  \usepackage{hyperref}
}

\mode<presentation>
{
  \setbeamertemplate{background canvas}[vertical shading][bottom=red!10,top=blue!10]
  \usetheme{BuildingBlocks}
  \usefonttheme[onlysmall]{structurebold}
}

\title{Building Blocks}
\author{Fausto Spoto}
\subtitle{Java and Android Concurrency}
\institute{Universit\`a di Verona, Italy}
\date{March 2017}

\setbeamercovered{invisible}

\begin{document}

\frame{\titlepage}

\begin{frame}
  \frametitle{Synchronized Collections in \texttt{java.util.}}

  \begin{itemize}
  \item \texttt{Vector}
  \item \texttt{Hashtable}
  \item\texttt{Collections.synchronizedCollection(collection)}
  \item\texttt{Collections.synchronizedSet(set)}
  \item\texttt{Collections.synchronizedSortedSet(sortedSet)}
  \item\texttt{Collections.synchronizedList(list)}
  \item\texttt{Collections.synchronizedMap(map)}
  \end{itemize}

  They encapsulate their state and synchronize every method.
  They commit to explicit support for client-side locking
  through their own intrinsic lock
\end{frame}

\begin{frame}[fragile]
  \frametitle{Compound Actions are not Atomic}

\begin{alltt}
\alert{@NotThreadSafe}
public class UnsafeVectorHelpers \{
  public static <T> T getLast(Vector<T> list) \{
    int lastIndex = list.size() - 1;  // \alert{read-modify}
    return list.get(lastIndex);       // \alert{write}
  \}

  public static <T> void deleteLast(Vector<T> list) \{
    int lastIndex = list.size() - 1;  // \alert{read-modify}
    list.remove(lastIndex);           // \alert{write}
  \}
\}
\end{alltt}

In a concurrent setting, this might throw an \texttt{ArrayIndexOutOfBoundsException}

\end{frame}

\begin{frame}[fragile]
  \frametitle{Make Compound Actions Atomic with Client-Side Locking}

{\small\begin{alltt}
\alert{@ThreadSafe}
public class SafeVectorHelpers \{
  public static <T> T getLast(Vector<T> list) \{
    \alert{synchronized (list)} \{
      int lastIndex = list.size() - 1;
      return list.get(lastIndex);
    \}
  \}

  public static <T> void deleteLast(Vector<T> list) \{
    \alert{synchronized (list)} \{
      int lastIndex = list.size() - 1;
      list.remove(lastIndex);
    \}
  \}
\}
\end{alltt}}

\end{frame}

\begin{frame}[fragile]
  \frametitle{Hidden Compound Actions in Iteration}

In a concurrent setting, the subsequent code might throw an \texttt{ArrayIndexOutOfBoundsException}:
  
\begin{alltt}
\alert{@NotThreadSafe}
for (int i = 0; i < vector.size(); i++)  // \alert{check}
  doSomething(vector.get(i));            // \alert{then-act}
\end{alltt}

How to fix it:

\begin{alltt}
\alert{@ThreadSafe}
\alert{synchronized (vector)} \{
  for (int i = 0; i < vector.size(); i++)
    doSomething(vector.get(i));          
\}
\end{alltt}

However, this solution prevents other threads from accessing the vector for the whole duration
of the iteration, which is undesirable

\end{frame}

\begin{frame}[fragile]
  \frametitle{\texttt{ConcurrentModificationException}}

  This exception is thrown by iterators on collections, if the original
  collection gets modified before the iteration ends. It occurs in
  a sequential setting as well:

{\small\begin{alltt}
for (E element: list)
  if (isBad(element))
    list.\alert{remove}(element);   // \alert{ConcurrentModificationException}
\end{alltt}}

In a concurrent setting, the exception might be thrown in
much more surprising situations:

{\small\begin{alltt}
\alert{@NotThreadSafe}
for (E element: vector) // may throw \alert{ConcurrentModificationException}
  System.out.println(element);
\end{alltt}}

\end{frame}

\begin{frame}[fragile]
  \frametitle{Hidden Compound Actions in Iteration}

The problem with iteration is that it is a hidden check-then-act operation:
  
\begin{alltt}
\alert{@NotThreadSafe}
for (E element: vector)          // \alert{check}
  System.out.println(element);   // \alert{then-act}
\end{alltt}

A way to fix it:

\begin{alltt}
\alert{@ThreadSafe}
\alert{synchronized (vector)} \{
  for (E element: vector)
    System.out.println(element);
\}
\end{alltt}

However, this solution prevents other threads from accessing the vector for the whole duration
of the iteration, which is undesirable

\end{frame}

\begin{frame}[fragile]
  \frametitle{Beware of Implicit Iteration}

{\small\begin{alltt}
\alert{@NotThreadSafe}
public class HiddenIterator \{

  @GuardedBy("this")
  private final Set<Integer> set = new HashSet<Integer>();

  public synchronized void add(Integer i) \{ set.add(i); \}
  public synchronized void remove(Integer i) \{ set.remove(i); \}

  public void addTenThings() \{
    Random r = new Random();
    for (int i = 0; i < 10; i++)
      add(r.nextInt());
    // \alert{missing synchronization below:}
    // \alert{this may throw a ConcurrentModificationException}  
    System.out.println("DEBUG: added ten elements to " + set);
  \}
\}
\end{alltt}}

\end{frame}

\begin{frame}
  \frametitle{Other Implicit Uses of Iteration on Collections}

  \begin{itemize}
  \item \texttt{hashCode()} and \texttt{equals()}, possibly called if a collection
    is used as an element of another collection
  \item \texttt{containsAll()}, \texttt{removeAll()}, \texttt{retainAll()}
  \item constructors that take a collection as parameter and build a copy
  \end{itemize}

  \begin{redbox}{}
    \begin{center}
      Always synchronize on the shared mutable collection before such operations
    \end{center}
  \end{redbox}
\end{frame}

\begin{frame}
  \frametitle{Concurrent Collections}

  \begin{greenbox}{Synchronized collections: \texttt{Vector}, \texttt{Hashtable}, \texttt{synchronizedXXX}}
    Thread-safety achieved by serializing all access: \alert{poor concurrency}
  \end{greenbox}

  \bigskip
  \begin{greenbox}{Concurrent collections: \texttt{ConcurrentHashMap}, \texttt{CopyOnWriteArrayList}}
    Multiple threads can concurrently access the collection in a thread-safe way:
    \alert{high concurrency, slightly increased memory footprint}
  \end{greenbox}

\end{frame}

\begin{frame}
  \frametitle{\texttt{ConcurrentHashMap implements ConcurrentMap}}

  Synchronized collections synchronize for the full duration of a simple \texttt{map.get(key)} operation,
  which might take longer than expected.
  \texttt{ConcurrentHashMap} instead uses \alert{lock striping}:

  \begin{itemize}
  \item readers can access the map concurrently
  \item readers and writers can access the map concurrently
  \item a limited number of writers can modify the map concurrently
  \item iterators are not fail-fast: they yield a snapshot of the map at the time of their creation
    and never throw a \texttt{ConcurrentModificationException}
  \item there is no need to synchronize during iteration
  \item there is no support for client-side locking
  \item there are extra atomic check-then-act operations
    \begin{itemize}
    \item \texttt{V putIfAbsent(K key, V value)}
    \item \texttt{boolean remove(K key, V value)}
    \item \texttt{boolean replace(K key, V oldValue, V newValue)}
    \item \texttt{V replace(K key, V newValue)}
    \end{itemize}
  \end{itemize}
\end{frame}

\begin{frame}
  \frametitle{\texttt{CopyOnWriteArrayList} and \texttt{CopyOnWriteArraySet}}

  They hold a backing collection that is recreated every time the collection gets modified:
  \begin{itemize}
  \item iterators refer to the backing collection at the time of creation, hence they reflect a snapshot
    of the collection at their creation time, are not fail-safe and never throw a
    \texttt{ConcurrentModificationException}
  \item there is no need to synchronize during iteration
  \item mutative methods are slow
  \end{itemize}

  \alert{They are the perfect concurrent data structure when modification is rare, while
  iteration is the predominant operation}

  \begin{itemize}
  \item such as for implementing a list of listeners, that must be iterated at each
    notification
  \end{itemize}
\end{frame}

\begin{frame}[fragile]
  \frametitle{Implementing a List of Listeners}

{\scriptsize\begin{alltt}
\alert{@ThreadSafe}    
public class VisualComponent \{
  private final List<KeyListener> keyListeners = new \alert{CopyOnWriteArrayList}<>();

  public void addKeyListener(KeyListener listener) \{
    keyListeners.add(listener);
  \}

  public void removeKeyListener(KeyListener listener) \{
    keyListeners.remove(listener);
  \}

  public void fireKeyListeners(KeyEvent event) \{
    // \alert{no need to synchronize here}
    \alert{for (KeyListener listener: keyListeners)}
      listener.keyPressed(event);
  \}
\}
\end{alltt}}

\end{frame}

\begin{frame}
  \frametitle{Blocking Queues}

  Blocking queues implement a FIFO or priority buffer with methods:

  \begin{itemize}
  \item \texttt{put(element)} that adds an element to the queue
    \begin{itemize}
    \item \alert{bounded} queues: blocks if full
    \item \alert{unbounded} queues: never blocks
    \end{itemize}
  \item \texttt{take()} that extracts the first element from the queue
    \begin{itemize}
    \item if the queue is empty, it blocks
    \end{itemize}
  \item \texttt{offer(element)} that adds an element to the queue
    \begin{itemize}
    \item \alert{bounded} queues: returns \texttt{false} if full
    \item \alert{unbounded} queues: equivalent to \texttt{put}
    \end{itemize}
  \item \texttt{poll()} that extracts the first element from the queue
    \begin{itemize}
    \item if the queue is empty, yields \texttt{null}
    \end{itemize}
  \item timed versions of \texttt{offer} and \texttt{poll}
  \end{itemize}

  Beware of unbounded queues that might fill up the memory
\end{frame}

\begin{frame}
  \frametitle{Producer/Consumer Pattern}

  \begin{center}
    \includegraphics[scale=.7,clip=false]{pictures/producer-consumer.png}
  \end{center}

\end{frame}

\begin{frame}
  \frametitle{Concurrent Queue Classes from the Java Library}

  \begin{center}
    \includegraphics[scale=.32,clip=false]{pictures/queues.png}
  \end{center}
  
\end{frame}

\begin{frame}[fragile]
  \frametitle{Example: File Crawling as Consumer/Producer}

{\small\begin{alltt}
private static final int BOUND = 10;
private static final int N_CONSUMERS
    = \alert{Runtime.getRuntime().availableProcessors()};

public static void startIndexing(File[] roots) \{
  BlockingQueue<File> queue = new LinkedBlockingQueue<>(BOUND);

  // \alert{start producers}
  for (File root: roots)
    new Thread(new FileCrawler(queue, root)).start();

  // \alert{start consumers}
  for (int i = 0; i < N_CONSUMERS; i++)
    new Thread(new Indexer(queue)).start();
\}
\end{alltt}}

\end{frame}

\begin{frame}[fragile]
  \frametitle{The Producer}

\vspace*{-1ex}
{\scriptsize\begin{alltt}
\alert{@ThreadSafe}
class FileCrawler implements Runnable \{
  private final BlockingQueue<File> fileQueue;
  private final File root;

  public FileCrawler(BlockingQueue<File> fileQueue, File root) \{
    this.fileQueue = fileQueue;
    this.root = root;
  \}
  ...
  public void run() \{
    crawl(root);
  \}

  private void crawl(File root) \{
    File[] entries = root.listFiles();
    if (entries != null)
      for (File entry : entries)
        if (entry.isDirectory())
          crawl(entry);
        else if (!alreadyIndexed(entry))
          fileQueue.put(entry);
  \}
\}
\end{alltt}}

\end{frame}

\begin{frame}[fragile]
  \frametitle{The Consumer}

{\small\begin{alltt}
\alert{@ThreadSafe}
class Indexer implements Runnable \{
  private final BlockingQueue<File> queue;

  public Indexer(BlockingQueue<File> queue) \{
    this.queue = queue;
  \}

  public void run() \{
    while (true)
      indexFile(queue.take());
  \}

  public void indexFile(File file) \{
    // index the file...
  \}
\}
\end{alltt}}

\end{frame}

\begin{frame}
  \frametitle{Blocking and Interruptible Methods}

  \begin{greenbox}{Cooperative interruption}
    A thread cannot be stopped. It is only possible
    to send an \alert{interruption request} to a thread
    by calling its \texttt{interrupt()} method. This will
    result in
    \begin{itemize}
    \item a checked \texttt{InterruptedException}, if the
      thread is blocked at a blocking method
      (such as \texttt{queue.take()})
    \item its interruption flag being set, otherwise. This can be checked
      through the \texttt{isInterrupted()} method of the thread
    \end{itemize}
  \end{greenbox}
\end{frame}

\begin{frame}[fragile]
  \frametitle{Dealing with \texttt{InterruptedException}}

\vspace*{-1ex}
The simplified code of two slides ago:

{\scriptsize\begin{alltt}
class Indexer implements Runnable \{
  ...
  public void run() \{
    while (true)
      indexFile(queue.take());
  \}
\}
\end{alltt}}

The real code:

{\scriptsize\begin{alltt}
class Indexer implements Runnable \{
  ...
  public void run() \{
    try \{
      while (true)
        indexFile(queue.take());
    \}
    \alert{catch (InterruptedException e)} \{
      \alert{// propagate back the interruption to the thread running this Runnable}
      \alert{Thread.currentThread().interrupt();}
    \}
  \}
\}
\end{alltt}}

\end{frame}

\begin{frame}[fragile]
  \frametitle{Never Swallow an Interruption}

\begin{greenbox}{Interruptions should not be eaten}
{\small\begin{alltt}
class Indexer implements Runnable \{
  ...
  public void run() \{
    try \{
      while (true)
        indexFile(queue.take());
    \}
    catch (InterruptedException e) \alert{\{\}} // don't do this at home
  \}
\}
\end{alltt}}
\end{greenbox}

\bigskip
This means that the request has been ignored. Instead
\begin{itemize}
\item either propagate back the exception
\item or propagate back the interruption request
\end{itemize}

Interruptions could be swallowed only if you are extending \texttt{Thread}!
\end{frame}

\begin{frame}
  \frametitle{Synchronizers}

  \begin{greenbox}{}
    A \alert{synchronizer} is any object that coordinates the control flow
    of threads based on its state:
    \begin{itemize}
    \item blocking queues
    \item latches
    \item semaphores
    \item barriers
    \item futures
    \end{itemize}
  \end{greenbox}
\end{frame}

\begin{frame}
  \frametitle{Latches}

  A \alert{latch} is a synchronizer that can delay the progress of threads until
  it reaches its \alert{terminal} state

  \begin{center}
    \includegraphics[scale=1.3,clip=false]{pictures/latch.png}
  \end{center}

  Class \texttt{CountDownLatch} allows threads to wait for a  set of events to occur. It is initialized
  to a positive number, representing the number of events to wait for
  \begin{itemize}
    \item method \texttt{countDown()} decrements the counter
    \item method \texttt{await()} blocks until the counter reaches zero
  \end{itemize}
\end{frame}

\begin{frame}[fragile]
  \frametitle{Example: Use Latches to Profile a Runnable}

\vspace*{-2.2ex}
{\scriptsize\begin{alltt}
public class TestHarness \{
  public long timeTasks(int nThreads, final Runnable task) throws InterruptedException \{
    final CountDownLatch startGate = \alert{new CountDownLatch(1)};
    final CountDownLatch endGate = \alert{new CountDownLatch(nThreads)};

    for (int i = 0; i < nThreads; i++)
      new Thread() \{
      	public void run() \{
          \alert{try \{
            startGate.await(); // wait for the GO!
            try \{
              task.run();
            \} finally \{
              endGate.countDown(); // finished!
            \}
          \} catch (InterruptedException ignored) \{\}}
        \}
      \}.start();

    long start = System.nanoTime();
    startGate.countDown();      // \alert{let all thread start now}
    endGate.await();            // \alert{wait for the last thread to finish}
    return System.nanoTime() - start;
  \}
\}
\end{alltt}}

\end{frame}

\begin{frame}
  \frametitle{Semaphores}

  A \alert{semaphore} is a synchronizer that controls the number of agents that can
  access a certain resource

  \begin{center}
    \includegraphics[scale=.12,clip=false]{pictures/semaphore.png}
  \end{center}

  Class \texttt{Semaphore} is initialized
  to a positive number, representing the number of available access \alert{permits}
  \begin{itemize}
  \item method \texttt{acquire()} grabs a permit and blocks if no permit is available
  \item method \texttt{release()} pushes back a permit
  \end{itemize}
\end{frame}

\begin{frame}[fragile]
  \frametitle{A Bounded Set with Blocking Addition}

\begin{alltt}
\alert{@ThreadSafe}
public class BoundedHashSet<T> \{
  private final Set<T> set;
  private final Semaphore sem;

  public BoundedHashSet(int bound) \{
    this.set = Collections.synchronizedSet(new HashSet<>());
    this.sem = \alert{new Semaphore(bound)};
  \}
\end{alltt}
  
\end{frame}

\begin{frame}[fragile]
  \frametitle{A Bounded Set with Blocking Addition}

\vspace*{-1ex}
{\small\begin{alltt}
  public boolean add(T o) throws InterruptedException \{
    sem.\alert{acquire}();
    boolean wasAdded = false;
    try \{
      wasAdded = set.add(o);
      return wasAdded;
    \} finally \{
      if (!wasAdded)
        sem.\alert{release}();
    \}
  \}

  public boolean remove(Object o) \{
    boolean wasRemoved = set.remove(o);
    if (wasRemoved)
      sem.\alert{release}();
    return wasRemoved;
  \}
\}
\end{alltt}}
  
\end{frame}

\begin{frame}
  \frametitle{Barriers}

  A \alert{barrier} is a synchronizer that blocks a group of threads
  until they have all reached a barrier point, then they proceed

  \begin{center}
    \includegraphics[scale=.35,clip=false]{pictures/barrier.png}
  \end{center}

  Class \texttt{CyclicalBarrier} is initialized to a positive number $n$,
  representing the number of threads that should be awaited at
  the barrier
  \begin{itemize}
  \item method \texttt{await()} blocks until the remaining $n-1$
    have called \texttt{await()} as well, at which moment all $n$ threads are
    allowed to proceed
  \end{itemize}

\end{frame}

\begin{frame}[fragile]
  \frametitle{The Backbone of a Cyclical Game}

{\scriptsize\begin{alltt}
\alert{@ThreadSafe}
public class CellularAutomata \{
  private final Board mainBoard;
  private final CyclicBarrier barrier;
  private final Worker[] workers;

  public CellularAutomata(Board board) \{
    int count = Runtime.getRuntime().availableProcessors();
    this.mainBoard = board;
    this.barrier = new CyclicBarrier(count, \alert{() -> mainBoard.commitNewValues()});
    this.workers = new Worker[count];
    for (int i = 0; i < count; i++) // \alert{split work between workers}
      workers[i] = new Worker(mainBoard.getSubBoard(count, i));
  \}

  public void start() \{
    // start all workers
    for (Worker worker: workers)
      new Thread(worker).start();

    mainBoard.waitForConvergence();
  \}
\end{alltt}}
  
\end{frame}

\begin{frame}[fragile]
  \frametitle{The Backbone of a Cyclical Game}

{\scriptsize\begin{alltt}
// inner class
private class Worker implements Runnable \{
  private final Board board;

  public Worker(Board board) \{ this.board = board; \}

  public void run() \{
    while (!board.hasConverged()) \{
      // compute the next generation of the values
      for (int x = 0; x < board.getMaxX(); x++)
        for (int y = 0; y < board.getMaxY(); y++)
          board.setNewValue(x, y, computeValue(x, y));

      try \{
        // \alert{wait until all other workers have finished}
        barrier.\alert{await}();
      \}
      catch (InterruptedException | BrokenBarrierException ex) \{
        return; // nobody can interrupt this
      \}
    \}
  \}
\}
\end{alltt}}
  
\end{frame}

\begin{frame}
  \frametitle{Latches, Semaphores and Barriers at a Glance}

  \begin{center}
  \begin{tabular}{|c|}
    \hline
    \textbf{Latch} \\\hline
    \texttt{new CountDownLatch(counter >= 0)}\\\hline
    \texttt{countDown()} decrements the counter\\\hline
    \texttt{await()} blocks until the counter reaches 0\\\hline\hline
    \textbf{Semaphore} \\\hline
    \texttt{new Semaphore(counter >= 0)}\\\hline
    \texttt{acquire()} decrements the counter and blocks if it is 0\\\hline
    \texttt{release()} increments the counter\\\hline\hline
    \textbf{Barrier} \\\hline
    \texttt{new CyclicBarrier(counter >= 0)}\\\hline
    \texttt{await()} decrements the counter and blocks if it is not 0.\\
    If it reaches 0, reset the counter to its initial value\\
    and wakes up all blocked threads\\\hline
  \end{tabular}
  \end{center}
  
\end{frame}

\begin{frame}
  \frametitle{Back to the Future}

  \begin{greenbox}{\texttt{Runnable}}
    The specification of a task that does not return any value nor throws any exception
  \end{greenbox}

  \bigskip
  \begin{greenbox}{\texttt{Callable<V>}}
    The specification of a task that can return a value of type \texttt{V} or throw an exception
  \end{greenbox}

    \bigskip
  \begin{greenbox}{\texttt{Future<V>}}
    A pointer to the future result of a computation, not necessarily terminated yet,
    that eventually will return a value of type \texttt{V} or throw an exception
  \end{greenbox}
  
\end{frame}

\begin{frame}[fragile]
  \frametitle{A Pointer to a Future Result}

\begin{alltt}
// specify the task to compute a V as a Callable<V> c
FutureTask<V> future = new FutureTask<>(c);

// build and start your worker
Thread thread = new Thread(future);
thread.start();

// do other things here
// and others things as well
// do more things

// eventually, ask for the result, \alert{when you need it}
V result = future.\alert{get}();
\end{alltt}
  
\end{frame}

\begin{frame}
  \frametitle{Getting the Result of a \texttt{FutureTask<V>}}

The statement \texttt{\alert{V result = future.get()}} might have many outcomes:

\begin{itemize}
\item the worker has already terminated and computed value $r$ $\Rightarrow$
  assign $r$ immediately to variable \texttt{result} and continue
\item the worker has already terminated by throwing an exception $e$ $\Rightarrow$
  throw immediately a new \texttt{ExecutionException} with cause $e$
\item the worker has been cancelled $\Rightarrow$ throw a new
  \texttt{CancellationException}
\item the worker is still working $\Rightarrow$ \alert{block} until:
  \begin{itemize}
  \item either the worker terminates by computing a value $r$ $\Rightarrow$
    wake up, assign $r$ to \texttt{result} and continue
  \item or the worker terminates by throwing an exception $e$ $\Rightarrow$
    wake up and throw a new \texttt{ExecutionException} with cause $e$
  \item or the blocked thread gets interrupted $\Rightarrow$ throw a new
    \texttt{InterruptedException}
  \end{itemize}
\end{itemize}

\end{frame}

\begin{frame}[fragile]
  \frametitle{An Example of the Use of \texttt{FutureTask}}

{\scriptsize\begin{alltt}
public class Preloader \{
  private final FutureTask<ProductInfo> future =
    new FutureTask<>(\alert{new Callable<ProductInfo>() \{
      public ProductInfo call() throws DataLoadException \{
        return loadProductInfo();
      \}
    \}});
  private final Thread thread = new Thread(future);

  public void start() \{ thread.start(); \}

  public ProductInfo get() throws DataLoadException, InterruptedException \{
    try \{
      return future.get();
    \} catch (ExecutionException e) \{
      Throwable cause = e.getCause();
      if (cause instanceof DataLoadException)
        throw (DataLoadException) cause;
      else
        throw launderThrowable(cause); // cast cause into unchecked
    \}
  \}
\}
\end{alltt}}

\end{frame}

\begin{frame}[fragile]
  \frametitle{Cast an Exception into an Unchecked Exception}

  \begin{itemize}
  \item \alert{unchecked exceptions}:
    instances of
    \texttt{RuntimeException} and \texttt{Error}. No need to declare them
    with \texttt{throws}
  \item \alert{checked exceptions}:
    all other exceptions. Must be declared with \texttt{throws}
  \end{itemize}

  If we know that \texttt{t} is an unchecked exception, the following code will
  throw it back as such:

{\small\begin{alltt}
public RuntimeException launderThrowable(Throwable t) \{
  if (t instanceof RuntimeException)
    return (RuntimeException) t;
  else if (t instanceof Error)
    throw (Error) t;
  else
    // this should never happen, since t was assumed unchecked
    throw new IllegalStateException("Not unchecked!", t);
\}
\end{alltt}}

\end{frame}

\begin{frame}
  \frametitle{\texttt{Runnable}, \texttt{Callable}, \texttt{Future} and \texttt{FutureTask}}

  \begin{center}
    \includegraphics[scale=.265,clip=false]{pictures/runnable.png}
  \end{center}

\end{frame}

\begin{frame}[fragile]
  \frametitle{Example: A Scalable Result Cache}

  Consider the specification of a function that transforms
  an \texttt{A} into a \texttt{V} and that might be
  arbitrarily expensive:

\begin{alltt}
interface Computable<A, V> \{
  V compute(A arg) throws InterruptedException;
\}
\end{alltt}

We want to define a decorator that adds a layer of caching to the process:

\begin{alltt}
computable2 = new Memoizer(computable1)
\end{alltt}

The function \texttt{computable2} is equivalent to
\texttt{computable1}, but caches repeated applications

\end{frame}

\begin{frame}[fragile]
  \frametitle{Attempt \#1: Synchronize on a Hash Map}

{\scriptsize\begin{alltt}
\alert{@ThreadSafe}
public class Memoizer1<A, V> implements Computable<A, V> \{
  @GuardedBy("this") private final Map<A, V> cache = new HashMap<>();
  private final Computable<A, V> c;

  public Memoizer1(Computable<A, V> c) \{
    this.c = c;
  \}

  public \alert{synchronized} V compute(A arg) throws InterruptedException \{
    V result = cache.get(arg);
    if (result == null) \{
      result = c.compute(arg);
      cache.put(arg, result);
    \}
    return result;
  \}
\}
\end{alltt}}

Only a computation can run at a time, in sequence!

\end{frame}

\begin{frame}[fragile]
  \frametitle{Attempt \#2: Use a Concurrent Map}

{\scriptsize\begin{alltt}
\alert{@ThreadSafe}
public class Memoizer2<A, V> implements Computable<A, V> \{
  private final Map<A, V> cache = new \alert{ConcurrentHashMap}<>();
  private final Computable<A, V> c;

  public Memoizer2(Computable<A, V> c) \{
    this.c = c;
  \}

  public V compute(A arg) throws InterruptedException \{
    V result = cache.get(arg);
    if (result == null) \{
      result = c.compute(arg);
      cache.put(arg, result);
    \}
    return result;
  \}
\}
\end{alltt}}

The same computation might be executed more times!

\end{frame}

\begin{frame}[fragile]
  \frametitle{Attempt \#3: Use a Concurrent Map and \texttt{FutureTask}}

\vspace*{-1.5ex}
{\scriptsize\begin{alltt}
\alert{@ThreadSafe}
public class Memoizer3<A, V> implements Computable<A, V> \{
  private final Map<A, Future<V>> cache = new ConcurrentHashMap<>();
  private final Computable<A, V> c;

  public Memoizer3(Computable<A, V> c) \{ this.c = c; \}

  public V compute(A arg) throws InterruptedException \{
    Future<V> f = cache.get(arg);
    if (f == null) \{
      FutureTask<V> ft = new FutureTask<>(\alert{() -> c.compute(arg)});
      cache.put(arg, f = ft);
      ft.run(); // call to c.compute happens here
    \}
    try \{
      return f.\alert{get}();
    \}
    catch (ExecutionException e) \{
      throw launderThrowable(e.getCause());
    \}
  \}
\}
\end{alltt}}

Very rarely, the same computation might be executed more times!

\end{frame}

\begin{frame}[fragile]
  \frametitle{Attempt \#4: Exploit \texttt{putIfAbsent} of Concurrent Maps}

\vspace*{-2.5ex}
{\scriptsize\begin{alltt}
\alert{@ThreadSafe}
public class Memoizer<A, V> implements Computable<A, V> \{
  private final Map<A, Future<V>> cache = new ConcurrentHashMap<>();
  private final Computable<A, V> c;
  public Memoizer(Computable<A, V> c) \{ this.c = c; \}

  public V compute(A arg) throws InterruptedException \{
    \alert{while (true)} \{
      Future<V> f = cache.get(arg);
      if (f == null) \{
        FutureTask<V> ft = new FutureTask<>(\alert{() -> c.compute(arg)});
        f = cache.\alert{putIfAbsent}(arg, ft);
        if (f == null) \{
          f = ft; ft.run();
        \}
      \}
      try \{
        return f.\alert{get}();
      \} catch (CancellationException e) \{
        cache.remove(arg, f); // cache eviction, while (true) will retry
      \} catch (ExecutionException e) \{
        throw launderThrowable(e.getCause());
      \}
    \}
  \}
\}
\end{alltt}}
\end{frame}

\begin{frame}[fragile]
  \frametitle{Use the Concurrent Cache in the Factorization Servlet}

{\scriptsize\begin{alltt}
\alert{@ThreadSafe}
@WebServlet("/Factorizer")
public class Factorizer extends StatelessFactorizer \{
  private static final long serialVersionUID = 1L;
  private final Computable<BigInteger, BigInteger[]> cache
    = \alert{new Memoizer<>(arg -> factor(arg))};

  @Override
  protected void doPost(HttpServletRequest request, HttpServletResponse response)
      throws IOException \{

    BigInteger number = extractFromRequest(request);

    try \{
      encodeIntoResponse(response, \alert{cache.compute}(number));
    \}
    catch (InterruptedException e) \{
      encodeErrorIntoResponse(response, "factorization interrupted");
    \}
  \}
\}
\end{alltt}}
  
\end{frame}

\begin{frame}[fragile]
  \frametitle{Exercise 1}

  Consider class \texttt{MD5s}, that implements a producer/consumer algorithm
  for generating the MD5 digests of files in some directories

  \begin{enumerate}
  \item complete method \texttt{index} of the \texttt{Indexer}, in order to store
    in memory and print on standard output the MD5 digest of each file. The digest
    can be computed through the auxiliary \texttt{md5} method
  \item complete methods \texttt{alreadyIndexed} and \texttt{getMD5sOf}
  \item complete method \texttt{stop} in order to interrupt all \texttt{Indexer}s.
    Pending work should be processed, hence implement the handler for
    \texttt{InterruptedException} inside the \texttt{run} method of the \texttt{Indexer}
  \end{enumerate}

  If everything works, it should be possible to invoke \texttt{MD5s} from the
  command line and print the MD5s of the specified directories:

{\scriptsize\begin{verbatim}
java -classpath examples/bin it.univr.concurrency.MD5s examples servlets
\end{verbatim}}

  The execution should terminate at the end of the computation of the digests, always with
  the same printout (but typically in different orders)

\end{frame}

\begin{frame}
  \frametitle{Exercise 2}

  Implement a board for the \texttt{CellularAutomata} example, in order to implement
  the \alert{Game of Life}. How do you implement the blocking
  \texttt{waitForConvergence()} method?

\end{frame}

\end{document}
