\documentclass[11pt]{beamer}  %% versione proiettore
%%\documentclass[11pt,handout]{beamer} %% versione stampa
\usepackage{lucidiJb-2ed}

\newcommand{\inputv}[1]{\check{{#1}}}
\newcommand{\outputv}[1]{\hat{{#1}}}

\mode<article>
{
  \usepackage{fullpage}
  \usepackage{hyperref}
}

\mode<presentation>
{
  \setbeamertemplate{background canvas}[vertical shading][bottom=red!10,top=blue!10]
  \usetheme{SwingMVC}
  \usefonttheme[onlysmall]{structurebold}
}

\title{Concurrency and GUIs}
\author{Fausto Spoto}
\subtitle{Java and Android Concurrency}
\institute{Universit\`a di Verona, Italy}
\date{March 2017}

\setbeamercovered{invisible}

\begin{document}

\frame{\titlepage}

\begin{frame}
\frametitle{Graphical Applications}

Graphical User Interfaces (GUI) interact with the user through widgets
%
\begin{itemize}
\item windows
\item buttons
\item labels
\item text fields
\item sliders
\item menus
\end{itemize}

The Swing and Android libraries implement such components through classical
design patterns: strategy, composite, decorator \ldots

\bigskip
\begin{redbox}{}
  \begin{center}
    For now, let us consider the simpler case of Swing programming
  \end{center}
\end{redbox}

\end{frame}

\begin{frame}
  \frametitle{Why Concurrency is Related to GUI Programming}

  \begin{greenbox}{GUI data structures are typically non-thread-safe}
    \begin{itemize}
    \item they cannot be used by more threads concurrently
    \item a special thread takes care of their management
      (main thread, user interface thread, event dispatch thread (EDT), graphical thread\ldots)
    \item long tasks cannot be executed in the user interface thread,
      but must be offlined to worker threads
    \item when worker threads complete, they usually have to schedule events
      for the EDT
    \end{itemize}
  \end{greenbox}
  
\end{frame}

\begin{frame}
  \frametitle{GUI Libraries Use Thread Confinement}

  GUI data structures are confined to the EDT:

  \begin{center}
    \includegraphics[width=10cm]{pictures/gui_app_structure.png}

    \medskip
    {\tiny Picture taken from \url{http://parallel.auckland.ac.nz/ParallelIT/PT_QuickStart.html}}
  \end{center}


\end{frame}

\begin{frame}
  \frametitle{Communication between EDT and Worker Threads}

  The EDT can schedule tasks on a worker thread by using any technique
  for spawning concurrent executions, such as:
  \begin{itemize}
  \item create and start a \texttt{Thread}
  \item submit the task to an executor
  \end{itemize}

  \bigskip
  The worker thread asks the EDT to run a task by calling the method
  \begin{center}
    \texttt{void java.awt.EventQueue.invokeLater(Runnable task)}
  \end{center}
  
\end{frame}

\begin{frame}\frametitle{A First Example of the Use of Swing}
%
\begin{center}
\includegraphics[width=7cm]{pictures/swing_hello_world.png}
\end{center}
%
\end{frame}

\begin{frame}\frametitle{Opening a New Window in Swing}
%
\begin{center}
\includegraphics[width=12cm]{pictures/swing_main.png}
\end{center}
%
\end{frame}

\begin{frame}\frametitle{A Frame Implements a Window and Contains Components}
%
\begin{center}
\includegraphics[width=10cm]{pictures/basic_frame.png}
\end{center}
%
\end{frame}

\begin{frame}\frametitle{A Component Knows how to Draw Itself}
%
\begin{center}
\includegraphics[width=12cm]{pictures/basic_component.png}
\end{center}
%
\end{frame}

\begin{frame}\frametitle{A Second Example of the Use of Swing}
%
\begin{center}
\includegraphics[width=7cm]{pictures/multipika.png}
\end{center}
%
\end{frame}

\begin{frame}\frametitle{A Second Example of the Use of Swing}
%
\begin{center}
\includegraphics[width=12cm]{pictures/tiles_component1.png}
\end{center}
%
\end{frame}

\begin{frame}\frametitle{A Second Example of the Use of Swing}
%
\begin{center}
\includegraphics[width=12cm]{pictures/tiles_component2.png}
\end{center}
%
\end{frame}

\begin{frame}\frametitle{A Third Example of the Use of Swing: Library Components}
%
\begin{center}
\includegraphics[width=6cm]{pictures/text_components.png}
\end{center}
%
\end{frame}

\begin{frame}\frametitle{TextFields, Labels, Container Panels\ldots}
%
\begin{center}
\includegraphics[width=12cm]{pictures/text_components1.png}
\end{center}
%
\end{frame}

\begin{frame}\frametitle{Text Areas, Scroll Bar Decorators\ldots}
%
\begin{center}
\includegraphics[width=12cm]{pictures/textarea.png}
\end{center}
%
\end{frame}

\begin{frame}\frametitle{Interactive Components}

\begin{center}
\includegraphics[width=11cm]{pictures/interactive_component.png}
\end{center}

A listener specifies the behavior of the click on the button:

\begin{center}
\includegraphics[width=11cm]{pictures/listener.png}
\end{center}

\begin{center}
  \alert{The Hollywood Principle: Don't call me, I'll call you}
\end{center}

\end{frame}

\begin{frame}\frametitle{Listeners as Explicit Classes (Name Pollution)}

\begin{center}
\includegraphics[width=12cm]{pictures/listener_explicit.png}
\end{center}

\end{frame}

\begin{frame}\frametitle{Listeners as Inner Classes (Still too Long)}

\begin{center}
\includegraphics[width=12cm]{pictures/listener_local_class.png}
\end{center}

\end{frame}

\begin{frame}\frametitle{Listeners as Anonymous Inner Classes (OK-ish)}

\begin{center}
\includegraphics[width=12cm]{pictures/listener_anonymous_class.png}
\end{center}

\end{frame}

\begin{frame}\frametitle{Listeners as Lambdas (only Java 8) (great!)}

\begin{center}
\includegraphics[width=12cm]{pictures/listener_lambda.png}
\end{center}

\end{frame}

\begin{frame}\frametitle{Buttons}
%
\begin{center}
\includegraphics[width=12cm]{pictures/text_components2.png}
\end{center}
%
\end{frame}

\begin{frame}\frametitle{A Serious Swing Application!}
%
\begin{center}
\includegraphics[width=11cm]{pictures/elections.png}
\end{center}
%
\end{frame}

\begin{frame}\frametitle{Elections: First View}

  We will develop a system with two interfaces, for handling election charts

  \begin{center}
    \includegraphics[width=6cm]{pictures/elections1.png}
  \end{center}

  \begin{itemize}
    \item by clicking on a party name one can increase its votes
    \item by clicking on the minus sign, one can remove a party
    \item by clicking on the plus sign, one can add a new party
  \end{itemize}

\end{frame}

\begin{frame}\frametitle{Elections: Second View}

  \begin{center}
    \includegraphics[width=6cm]{pictures/elections2.png}
  \end{center}

  \begin{itemize}
  \item by clicking on a party name one can increase its votes
  \item no provision for adding/removing parties
  \end{itemize}

\end{frame}

\begin{frame}\frametitle{Separation of Concerns}

The graphical interface should be kept separate from the logic:
\begin{itemize}
\item for distinct versions
\item for desktop
\item for Android
\item for special accessibility
\end{itemize}

\bigskip
Data should be kept separate from the logic:
\begin{itemize}
\item faster on desktop
\item more compact on mobile
\item kept in a database
\item accessible through a web interface
\end{itemize}

\end{frame}

\begin{frame}\frametitle{The Model-View-Controller Design Pattern}

\begin{center}
\includegraphics[width=10cm]{pictures/mvc.jpg}
\end{center}

\end{frame}

\begin{frame}\frametitle{The Organization into Packages}

\begin{itemize}
\item \texttt{model}: data representation
\item \texttt{view}: game views
\item \texttt{controller}: data/view coordination
\end{itemize}

\bigskip
In order to clarify which thread can call which method, we can annotate the latter
as follows:

\begin{itemize}
\item \texttt{@UiThread}: for methods that can only be executed by the EDT
\item \texttt{@WorkerThread}: for methods that cannot be executed by the EDT
\end{itemize}

\begin{redbox}{}
  Current compilers do not check such annotations, but static analyzers are starting
  checking them
\end{redbox}

\end{frame}

\begin{frame}\frametitle{The MVC Triple 1/2}

  \begin{center}
    \includegraphics[width=12cm]{pictures/mvc1.png}
  \end{center}

\end{frame}

\begin{frame}\frametitle{The MVC Triple 2/2}

  \begin{center}
    \includegraphics[width=10cm]{pictures/mvc2.png}
  \end{center}

\end{frame}

\begin{frame}\frametitle{The Starting Point of the Program}

    \begin{center}
      \includegraphics[width=11cm]{pictures/main.png}
    \end{center}
    
\end{frame}

\begin{frame}\frametitle{The Model}

\begin{center}
\includegraphics[width=12cm]{pictures/model.png}
\end{center}

\end{frame}

\begin{frame}\frametitle{The Model: Implementation}

\begin{center}
\includegraphics[width=8.5cm]{pictures/model_implementation.png}
\end{center}

\end{frame}

\begin{frame}\frametitle{The Controller}

\begin{center}
\includegraphics[width=8.5cm]{pictures/controller.png}
\end{center}

\end{frame}

\begin{frame}\frametitle{The View}

\begin{center}
\includegraphics[width=6.5cm]{pictures/view.png}
\end{center}

\end{frame}

\begin{frame}\frametitle{The View: First Implementation 1/4}

\begin{center}
\includegraphics[width=12cm]{pictures/view1-1.png}
\end{center}

\end{frame}

\begin{frame}\frametitle{The View: First Implementation 2/4}

\begin{center}
\includegraphics[width=12cm]{pictures/view1-2.png}
\end{center}

\end{frame}

\begin{frame}\frametitle{The View: First Implementation 3/4}

\begin{center}
\includegraphics[width=12cm]{pictures/view1-3.png}
\end{center}

\end{frame}

\begin{frame}\frametitle{The View: First Implementation 4/4}

\begin{center}
\includegraphics[width=9.3cm]{pictures/view1-4.png}
\end{center}

\end{frame}

\begin{frame}\frametitle{A Custom Component for Histograms}

  \begin{center}
    \includegraphics[width=10.5cm]{pictures/histogram.png}
  \end{center}

\end{frame}

\begin{frame}\frametitle{The View: Second Implementation 1/3}

\begin{center}
\includegraphics[width=12cm]{pictures/view2-1.png}
\end{center}

\end{frame}

\begin{frame}\frametitle{The View: Second Implementation 2/3}

\begin{center}
\includegraphics[width=10cm]{pictures/view2-2.png}
\end{center}

\end{frame}

\begin{frame}\frametitle{The View: Second Implementation 3/3}

\begin{center}
\includegraphics[width=12cm]{pictures/view2-3.png}
\end{center}

\end{frame}

\begin{frame}[fragile]
  \frametitle{Exercise 1: Generate Votes through an Election Server}

  \begin{greenbox}{}
    Write and publish a servlet with a single method:

    \begin{center}
      \texttt{sendvotes?howmany=XXX\&parties=P1,P2,...,Pn}
    \end{center}

  that sends back \texttt{howmany} random votes for the given, comma-separated party names.
  Votes are reported back to the client
  as a sequence of \texttt{howmany} party names from \texttt{P1,P2,...,Pn},
  each standing for a vote for that party:
\begin{verbatim}
flower party
great party
PCDD
great party
new party
flower party
great party
...
\end{verbatim}
  \end{greenbox}

\end{frame}

\begin{frame}
  \frametitle{Exercise 2: Import Votes from an Election Server}

  \begin{greenbox}{}
    Modify both views for the election example, by adding a button
    \emph{import votes from server} that accesses the previous servlet
    and registers $1000$ random votes generated by the server for the
    currently registered parties. This will
    modify the model and update the views
  \end{greenbox}

  \bigskip
  \begin{redbox}{You Need a Worker Thread}
    The connection to the servlet cannot be executed in the EDT, since it might
    be slow and is potentially blocking. Spawn a thread instead and report the results
    back to the EDT at the end
  \end{redbox}
  
  \bigskip
  \begin{greenbox}{}
    Is it wise to generate an EDT event for each vote that gets read and registered to
    the model? Is it possible instead to batch all updates into a restricted number of EDT events?
    What about making the model thread-safe without thread confinement?
  \end{greenbox}

\end{frame}

\begin{frame}\frametitle{Exercise 3: Third Implementation of the Elections View}

  \begin{greenbox}{}
    Implement a third version of the view, where the names of the parties are reported
    in the top part of the frame, in distinct colors, and below is reported
    a pie chart, with colors corresponding to those of the parties. No provision for
    adding or removing parties
  \end{greenbox}

  \begin{center}
    See a pie chart Swing component at:
    
    \texttt{http://blue-walrus.com/2012/09/simple-pie-chart-in-java-swing}
  \end{center}
\end{frame}

\end{document}
