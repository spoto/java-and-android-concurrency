\documentclass[11pt]{beamer}  %% versione proiettore
%%\documentclass[11pt,handout]{beamer} %% versione stampa
\usepackage{lucidiJb-2ed}

\mode<article>
{
  \usepackage{fullpage}
  \usepackage{hyperref}
}

\mode<presentation>
{
  \setbeamertemplate{background canvas}[vertical shading][bottom=red!10,top=blue!10]
  \usetheme{TaskExecution}
  \usefonttheme[onlysmall]{structurebold}
}

\title{Task Execution}
\author{Fausto Spoto}
\subtitle{Java and Android Concurrency}
\institute{Universit\`a di Verona, Italy}
\date{March 2017}

\setbeamercovered{invisible}

\begin{document}

\frame{\titlepage}

\begin{frame}[fragile]
  \frametitle{Single Thread Scheduling of Task}

  Many concurrent applications are organized around the idea of the
  execution of tasks, that is, of (likely independent) units of work

  Tasks can be executed on a single thread:

{\small\begin{alltt}
\alert{@ThreadSafe}
public class SingleThreadWebServer \{
  public static void main(String[] args) throws IOException \{
    try (ServerSocket socket = new ServerSocket(80)) \{
      while (true) \{
        Socket connection = socket.accept();
        handleRequest(connection);
      \}
    \}
  \}
\}
\end{alltt}}

\begin{greenbox}{}
  It is the simplest scheduling policy, which avoids any concurrency problem,
  but it is of course rather inefficient
\end{greenbox}

\end{frame}

\begin{frame}[fragile]
  \frametitle{Explicit Creation of Threads per Task}

  Better responsiveness can be achieved by starting a new thread for each task:

{\small\begin{alltt}
\alert{@ThreadSafe}
public class ThreadPerTaskWebServer \{
  public static void main(String[] args) throws IOException \{
    try (ServerSocket socket = new ServerSocket(80)) \{
      while (true) \{
        Socket connection = socket.accept();
        Runnable task = () -> handleRequest(connection);
        \alert{new Thread(task).start()};
      \}
    \}
  \}
\}
\end{alltt}}

\begin{greenbox}{}
  Better responsiveness, parallel execution of CPU bound and I/O bound tasks. \alert{Tasks must be thread-safe!}
\end{greenbox}

\end{frame}

\begin{frame}
  \frametitle{Drawbacks of Unbound Thread Creation}

  The simplicity of \texttt{ThreadPerTaskWebServer} comes at some price:

  \begin{itemize}
  \item \alert{thread lifecycle overhead}: creating and destroying threads is slow
  \item \alert{resource consumption}: creating more runnable threads than available CPUs costs memory
    without any efficiency gain
  \item \alert{there is a limit on the number of threads}. Trespassing that limit brings the application
    to an out-of-memory exception and the server to a denial-of-service
  \end{itemize}

  \bigskip
  \begin{greenbox}{}
    Up to a certain point, more threads can improve throughput, but beyond that point
    creating more threads just slows down the application, and creating one thread too many
    can cause the entire application to crash horribly
  \end{greenbox}

\end{frame}

\begin{frame}
  \frametitle{The \texttt{Executor} Framework}

  \begin{center}
    \includegraphics[scale=.201,clip=false]{pictures/executors.png}
  \end{center}
\end{frame}

\begin{frame}[fragile]
  \frametitle{Using an Executor for a Parallel Web Service}

{\small\begin{alltt}
\alert{@ThreadSafe}
public class TaskExecutionWebServer \{
  private static final int NTHREADS = 100;
  private static final \alert{Executor exec
    = Executors.newFixedThreadPool(NTHREADS)};

  public static void main(String[] args) throws IOException \{
    try (ServerSocket socket = new ServerSocket(80)) \{
      while (true) \{
        Socket connection = socket.accept();
        Runnable task = () -> handleRequest(connection);
        \alert{exec.execute}(task);
      \}
    \}
  \}
\}
\end{alltt}}
  
\end{frame}

\begin{frame}
  \frametitle{Thread Pools}

  Thread pools are the implementation of an executor:
  \begin{itemize}
  \item a set of threads, ready to run tasks
  \item a queue holding the task to process
  \end{itemize}

  \bigskip
  \begin{greenbox}{Advantages}
    \begin{itemize}
    \item threads are recycled, hence amortizing creation and teardown costs
    \item the right number of threads allows one to keep the processor busy
      without running into out of memory
    \end{itemize}
  \end{greenbox}

  \bigskip
  \begin{greenbox}{Creation of Thread Pools}
    \begin{itemize}
    \item by using the factory methods of \texttt{Executors}, for \alert{predefined configurations} (preferred)
    \item by using the constructors of \texttt{ThreadPoolExecutor}, for \alert{fine-grained configuration}
    \end{itemize}
  \end{greenbox}
  
\end{frame}

\begin{frame}
  \frametitle{Predefined Thread Pool Configurations from \texttt{Executors}}

  \begin{greenbox}{\texttt{Executors.newFixedThreadPool(int nThreads)}}
    yields a thread pool that creates threads up to the given limit and adds
    a new thread if one dies
  \end{greenbox}

  \bigskip
  \begin{greenbox}{\texttt{Executors.newCachedThreadPool()}}
    yields an unboound thread pool, whose size grows and shrinks with the
    number of pending tasks
  \end{greenbox}

  \bigskip
  \begin{greenbox}{\texttt{Executors.newSingleThreadExecutor()}}
    yields a thread pool with a single thread, that executes tasks sequentially. It
    replaces this only thread if it dies
  \end{greenbox}

  \bigskip
  \begin{greenbox}{\texttt{Executors.newScheduledThreadPool(int corePoolSize)}}
    yields a thread pool with the given number of threads, able to schedule
    delayed or periodic tasks
  \end{greenbox}
  
\end{frame}

\begin{frame}
  \frametitle{\texttt{ExecutorService}'s Shutdown}

  The JVM does not terminate until the last non-daemon thread has terminated

  \bigskip
  \begin{redbox}{}
    \begin{center}
      Creating a thread pool prevents JVM's termination\\
      Thread pools must be shut down when no longer in use
    \end{center}
  \end{redbox}

  \bigskip
  An \texttt{ExecutorService} transitions through the following states (from left to right,
  never backwards):

  \begin{center}
    \emph{running} $\Rightarrow$ \emph{shutting down} $\Rightarrow$ \emph{terminated}
  \end{center}
  
\end{frame}

\begin{frame}
  \frametitle{\texttt{ExecutorService}'s Lifecycle Methods}

  \begin{greenbox}{\texttt{void shutdown()}: \emph{running} $\Rightarrow$ \emph{shutting down}}
    \alert{Graceful shutdown}: wait for all pending tasks to finish but don't accept any more work.
    At the end, transition to the \emph{terminated} state
  \end{greenbox}

  \bigskip
  \begin{greenbox}{\texttt{List<Runnable> shutdownNow()}: \emph{running} $\Rightarrow$ \emph{shutting down}}
    \alert{Abrupt shutdown}: interrupt the running tasks, wait for them to finish,
    don't accept any more work and return the list of pending but not yet run tasks.
    At the end, transition to the \emph{terminated} state
  \end{greenbox}

  \bigskip
  \begin{greenbox}{\texttt{boolean isShutdown()}}
    Yields true if and only if the executor service is in the \emph{shutting down} state
  \end{greenbox}

  \bigskip
  \begin{greenbox}{\texttt{boolean isTerminated()}}
    Yields true if and only if the executor service is in the \emph{terminated} state
  \end{greenbox}
  
\end{frame}

\begin{frame}
  \frametitle{Wait for the Termination of an \texttt{ExecutorService}}

  \begin{greenbox}{\texttt{boolean awaitTermination(long timeout, TimeUnit unit) throws InterruptedException}}
    Blocks and waits the given amount of time until the executor service reaches the \emph{terminated} state
    \begin{itemize}
    \item if the \emph{terminated} state is reached by the given timeout: yields true
    \item if the \emph{terminated} state is not reached by the given timeout: yields false
    \item if the current (waiting) thread is interrupted before the given timeout: throws
      an \texttt{InterruptedException}
    \end{itemize}
  \end{greenbox}
  
\end{frame}

\begin{frame}
  \frametitle{Rejected Tasks}

  What happens if a new, incoming task is submitted to an executor service
  in the \emph{shutting down} or \emph{terminated} state?

  \bigskip
  \begin{redbox}{}
    \begin{center}
      \texttt{RejectedExecutionException}
    \end{center}
  \end{redbox}

  \bigskip
  This behavior can be changed by specifying a suitable
  \alert{rejected execution handler}

\end{frame}

\begin{frame}[fragile]
  \frametitle{Stoppable Web Server}

{\scriptsize\begin{alltt}
\alert{@ThreadSafe}
public class LifecycleWebServer \{
  private final ExecutorService exec = Executors.newCachedThreadPool();

  public void start() throws IOException \{
    try (ServerSocket socket = new ServerSocket(80)) \{
      \alert{while (!exec.isShutdown())}
        try \{
          Socket connection = socket.accept();
          exec.execute(() -> handleRequest(connection));
        \}
        \alert{catch (RejectedExecutionException e)} \{
          log("task submission rejected", e);
        \}
    \}
  \}

  public void stop() \{
    \alert{exec.shutdown}(); // graceful shutdown
  \}
\}
\end{alltt}}
  
\end{frame}

\begin{frame}[fragile]
  \frametitle{Delayed and Period Tasks}

  \begin{redbox}{}
    \begin{center}
      Do not use a \texttt{java.util.TimerTask}
    \end{center}
  \end{redbox}
  \begin{greenbox}{}
    The legacy class \texttt{TimerTask} has serious issues that make it
    deprecated nowadays (a single thread for \emph{all} \texttt{TimerTask}s,
    non-replaceable)
  \end{greenbox}

  \bigskip
  Instead, use a \texttt{ScheduledExecutorService} and its scheduling methods

{\small\begin{alltt}
ScheduledExecutorService exec
           = \alert{Executors.newScheduledThreadPool}(nThreads);

// \alert{run after 10 milliseconds from now}
exec.\alert{schedule}(task, 10L, TimeUnit.MILLISECONDS);

// \alert{run after 10 milliseconds from now and then every second}
exec.\alert{scheduleAtFixedRate}(task, 10L, 1000L, TimeUnit.MILLISECONDS);
\end{alltt}}
  
\end{frame}

\begin{frame}[fragile]
  \frametitle{Example: HTML Page Rendering}

  The sequential approach is simple and does not use any executor. However,
  downloading all images might take a while, only then are images rendered

{\small\begin{alltt}
public class SingleThreadRenderer \{
  void renderPage(CharSequence source) \{
    \alert{// render the text}
    renderText(source);

    \alert{// download all images}
    List<ImageData> imageData = new ArrayList<>();
    for (ImageInfo imageInfo: scanForImageInfo(source))
      imageData.add(imageInfo.downloadImage());

    \alert{// render the images}
    for (ImageData data: imageData)
      renderImage(data);
  \}
\}
\end{alltt}}
  
\end{frame}

\begin{frame}[fragile]
  \frametitle{Split Page Rendering in Two Concurrent Tasks}

\vspace*{-1ex}
{\scriptsize\begin{alltt}
public abstract class FutureRenderer \{
  private final ExecutorService executor = \alert{Executors.newCachedThreadPool()};

  void renderPage(CharSequence source) \{
    final List<ImageInfo> imageInfos = scanForImageInfo(source);
    Callable<List<ImageData>> task = () -> \{
      List<ImageData> result = new ArrayList<>();
      for (ImageInfo imageInfo: imageInfos)
        result.add(imageInfo.downloadImage());
      return result;
    \};
    Future<List<ImageData>> \alert{future = executor.submit(task)};
    renderText(source); // in parallel with image downloads
    try \{
      for (ImageData data: \alert{future.get()})
        renderImage(data);
    \} catch (InterruptedException e) \{
      Thread.currentThread().interrupt();
      future.cancel(true);
    \} catch (ExecutionException e) \{
      throw launderThrowable(e.getCause());
    \}
  \}
\}
\end{alltt}}
  
\end{frame}

\begin{frame}
  \frametitle{Limitations of Splitting Heterogeneous Tasks}

  \begin{redbox}{}
    If heterogeneous tasks require very different times, running them in parallel might not
    be worthwhile and complicates the code
  \end{redbox}

  \bigskip
  \begin{greenbox}{}
    The real performance payoff of dividing a program's workload into tasks
    comes when there are a large number of independent, \emph{homogeneous} tasks
    that can be processed concurrently
  \end{greenbox}
  
\end{frame}

\begin{frame}
  \frametitle{Downloading Each Image in Parallel}

  A much faster and reactive page renderer would download each page in its own thread
  and render the image as soon as its download finished.

  \medskip
  A \alert{completion service} is a bridge between an executor and a blocking queue
  \begin{itemize}
  \item tasks submitted to the completion service are delegated to the executor
  \item when a task finishes, its future is put in the blocking queue
  \end{itemize}

  \bigskip
  \begin{greenbox}{}
    By taking futures from the queue, one gets fully computed futures, whose
    \texttt{get()} method will not block
  \end{greenbox}
  
\end{frame}

\begin{frame}[fragile]
  \frametitle{Render Each Image As Soon As It Becomes Available}

\vspace*{-1ex}
{\scriptsize\begin{alltt}
public abstract class Renderer \{
  private final ExecutorService executor;

  Renderer(ExecutorService executor) \{
    this.executor = executor;
  \}

  void renderPage(CharSequence source) \{
    List<ImageInfo> info = scanForImageInfo(source);
    CompletionService<ImageData> completionService =
      \alert{new ExecutorCompletionService<>(executor)};
    for (ImageInfo imageInfo: info)
      \alert{completionService.submit}(() -> imageInfo.downloadImage());
    renderText(source);
    try \{
      for (int t = 0, n = info.size(); t < n; t++) \{
        Future<ImageData> f = \alert{completionService.take}();
        renderImage(f.get());
      \}
    \}
    catch (InterruptedException e) \{ Thread.currentThread().interrupt(); \}
    catch (ExecutionException e) \{ throw launderThrowable(e.getCause()); \}
  \}
\}
\end{alltt}}
  
\end{frame}

\begin{frame}[fragile]
  \frametitle{Running a Task with a Time Limit}

\vspace*{-1ex}
{\scriptsize\begin{alltt}
public class RenderWithTimeBudget \{
  private static final Ad DEFAULT_AD = new Ad();
  private static final long TIME_BUDGET = 1000;
  private static final ExecutorService exec = Executors.newCachedThreadPool();

  Page renderPageWithAd() throws InterruptedException \{
    long endNanos = System.nanoTime() + TIME_BUDGET;
    Future<Ad> f = exec.submit(new FetchAdTask());
    Page page = renderPageBody(); // render the page while waiting for the ad
    Ad ad;
    try \{
      // Only wait for the remaining time budget
      long timeLeft = endNanos - System.nanoTime();
      ad = \alert{f.get(timeLeft, TimeUnit.NANOSECONDS)};
    \}
    catch (ExecutionException e) \{ ad = DEFAULT_AD; \}
    catch (TimeoutException e) \{
      ad = DEFAULT_AD;
      \alert{f.cancel(true)}; // useful if the task reacts to cancellation
    \}
    page.setAd(ad);
    return page;
  \}
\}
\end{alltt}}
  
\end{frame}

\begin{frame}
  \frametitle{Running More Tasks with a Time Limit}

  The typical travel reservation portal:
  \begin{itemize}
  \item it contacts many travel providers concurrently
  \item if they answer under the time limit, their quote is reported
  \item after the time limit expires, the travel provider is discarded
  \end{itemize}

  \begin{center}
    \includegraphics[scale=.25,clip=false]{pictures/flights.png}
  \end{center}

\end{frame}

\begin{frame}[fragile]
  \frametitle{Asking More Travel Quotes under a Time Limit}

\vspace*{-2ex}
{\scriptsize\begin{alltt}
public class TimeBudget \{
  private static ExecutorService exec = Executors.newCachedThreadPool();

  public List<Quote> getRankedQuotes(Travel travel, Set<Company> companies,
     Comparator<Quote> ranking) throws InterruptedException \{
    List<QuoteTask> tasks = new ArrayList<>();
    for (Company company: companies)
      tasks.add(new QuoteTask(company, travel));
    \alert{List<Future<Quote>> futures = exec.invokeAll(tasks, 10000, MILLISECONDS)};
    List<Quote> quotes = new ArrayList<>(tasks.size());
    Iterator<QuoteTask> taskIter = tasks.iterator();
    \alert{for (Future<Quote> f: futures)} \{
      QuoteTask task = taskIter.next();
      try \{
        quotes.add(\alert{f.get()});
      \} catch (ExecutionException e) \{
        quotes.add(task.getFailureQuote(e.getCause()));
      \} catch (CancellationException e) \{
        quotes.add(task.getTimeoutQuote(e));
      \}
    \}
    Collections.sort(quotes, ranking);
    return quotes;
  \}
\}
\end{alltt}}
  
\end{frame}

\begin{frame}
  \frametitle{Exercise: Matrix Multiplication with an Executor}

  Consider the parallel multiplication again. Use an executor to avoid repeated
  threads creation and teardown

  \bigskip
  How much faster is the resulting implementation? How much higher is the CPU
  utilization average?

  \bigskip
  Use \alert{the same} executor also in the constructor of random matrices. Make
  that random construction parallel. What happens to the execution time now?
  Are you sharing a data structure among the threads? Is it thread-safe?
  Must it really be shared?

\end{frame}

\end{document}
